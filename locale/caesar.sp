{"##0_entertainment_access##":"Esta vivienda no tiene acceso a ningún tipo de entretenimiento."
"##1_entertainment_access##":"Esta vivienda apenas tiene acceso a entretenimientos."
"##2_entertainment_access##":"Esta vivienda tiene algo de acceso a actividades de entretenimiento."
"##3_entertainment_access##":"Esta vivienda tiene acceso muy limitado a actividades de entretenimiento."
"##4_entertainment_access##":"Esta vivienda tiene acceso razonable a actividades de entretenimiento."
"##5_entertainment_access##":"Esta vivienda tiene acceso limitado a actividades de entretenimiento."
"##6_entertainment_access##":"Esta vivienda tiene buen acceso a actividades de entretenimiento."
"##7_entertainment_access##":"Esta vivienda tiene muy buen acceso a actividades de entretenimiento."
"##8_entertainment_access##":"Esta vivienda tiene un excelente acceso a actividades de entretenimiento."
"##9_entertainment_access##":"Esta vivienda tiene acceso a todas las actividades de entretenimiento que desee."
"##academies##":"Academias"
"##academy_full_work##":"Esta academia está en funcionamiento. Los jóvenes locales se instruyen."
"##academy_no_workers##":"La academia no está en funcionamiento, y no sirve a la comunidad local"
"##academy_trained##":"Entrenado en academia"
"##academy##":"Academia"
"##accept_deity_status##":"¡Acepta el status de deidad!"
"##accept_goods##":"Aceptar bienes"
"##accept_promotion##":"Aceptar ascenso"
"##accept##":"Aceptar"
"##accepting##":"Aceptar"
"##access_ramp##":"Acceso rampa"
"##actor_average_life##":"La vida aquí es encantadora."
"##actor_gods_angry##":"¡Aaagh! ¡Los dioses se han enfadado! ¡Estamos condenados!"
"##actor_good_life##":"La ciudad no está mal."
"##actor_high_workless##":"Hay tanto paro que no puedo aprenderme bien mi papel."
"##actor_low_entertainment##":"Trabajo todo lo que puedo, pero hay una gran falta de diversión en la ciudad."
"##actor_need_workers##":"No hay suficientes trabajadores en la ciudad."
"##actor_so_hungry##":"No puedo actuar bien si no tengo alimento."
"##actor##":"Actor"
"##actorColony_bad_work##":"Soy el único personal laboral aquí. No puedo trabajar en estas condiciones. En el mejor de los caos, puedo formar a un actor en tres meses."
"##actorColony_full_work##":"Nos satisface comunicar que con tanto personal estamos formando hasta a cuatro actores nuevos al mes."
"##actorColony_need_some_workers##":"Estamos algo escasos de personal, por lo que no podemos formar a más de dos actores al mes."
"##actorColony_no_workers##":"Esta colonia está desierta. Sin mentores no pueden formarse nuevos actores."
"##actorColony_patrly_workers##":"Sólo tenemos a la mitad de personal, con lo que sólo podremos formar a un actor este mes."
"##actorColony_slow_work##":"Necesitamos personal urgentemente. No será fácil formar a un actor en los próximos dos meses."
"##actorColony##":"Colonia de actores"
"##ad##":"DC"
"##adjust_exact##":"Ajustar la cantidad exacta que deseas donar"
"##adjust_tax_rate##":"Modificar el porcentaje de impuestos de la ciudad"
"##administration_building##":"Estructuras gubernamentales o administrativas"
"##advanced_houseinfo##":"Información detallada sobre esta casa"
"##advchief_crime##":"Criminalidad"
"##advchief_education##":"Educación"
"##advchief_employers_ok##":"La ciudad no tiene problemas de empleo"
"##advchief_employment##":"Trabajo"
"##advchief_finance##":"Economía"
"##advchief_food_consumption##":"Consumo de alimentos"
"##advchief_food_stocks##":"Reservas de comida"
"##advchief_havedeficit##":"Reducción beneficios este año:"
"##advchief_haveprofit## ":"Incremento beneficios este año:"
"##advchief_health_awesome_clinic##":"La sanidad pública es excelente, sin listas de espera para acudir a las consultas de la zona."
"##advchief_health_awesome##":"La sanidad pública es excelente."
"##advchief_health_bad_clinic##":"La sanidad pública es mala. Las consultas están saturadas y temen que haya una epidemia mortal."
"##advchief_health_bad##":"La sanidad pública es mala."
"##advchief_health_good_clinic##":"La sanidad pública es casi perfecta. Las consultas están prácticamente vacías."
"##advchief_health_good##":"La sanidad pública es casi perfecta."
"##advchief_health_high_clinic##":"La sanidad pública es buena. Los ciudadanos únicamente son propensos a pequeñas dolencias."
"##advchief_health_high##":"La sanidad pública es buena."
"##advchief_health_less_clinic##":"La sanidad pública es deficiente. Para mejorarla se necesitarían consultas y abundancia de alimentos."
"##advchief_health_less##":"La sanidad pública es deficiente."
"##advchief_health_low##":"La sanidad pública es malísima."
"##advchief_health_lower##":"¡La sanidad pública es malísima! La peste está asegurada en estas condiciones."
"##advchief_health_middle_clinic##":"La sanidad pública es regular. Las consultas mantienen bajo control las enfermedades."
"##advchief_health_middle##":"La sanidad pública es regular."
"##advchief_health_perfect_clinic##":"La sanidad pública es perfecta. Las consultas vacías son un modelo a seguir para el resto del Imperio."
"##advchief_health_perfect##":"La sanidad pública es perfecta."
"##advchief_health_simple_clinic##":"La sanidad pública está por debajo de la media."
"##advchief_health_simple##":"La sanidad pública está por debajo de la media. Asegúrate de que los ciudadanos tengan comida y acceso a consultas."
"##advchief_health_terrible_clinic##":"¡La sanidad pública es muy mala! Las consultas no dan abasto y las enfermedades son inevitables."
"##advchief_health_terrible##":"La sanidad pública es muy mala."
"##advchief_health_verygood_clinic##":"La sanidad pública es muy buena. Las consultas locales llevan bien la situación."
"##advchief_health_verygood##":"La sanidad pública es muy buena."
"##advchief_health##":"Sanidad"
"##advchief_high_crime_in_district##":"Algunas zonas tienen altos índices de criminalidad"
"##advchief_high_crime##":"El crimen se ha extendido por la ciudad."
"##advchief_low_crime##":"Hay poca criminalidad, nada serio."
"##advchief_military##":"Ejército"
"##advchief_needworkers##":"La ciudad está escasa de"
"##advchief_sentiment##":"Percepción ciudadana"
"##advchief_some_need_academy##":"Algunos ciudadanos desean más colegios"
"##advchief_some_need_barber##":"Algunos ciudadanos necesitan barberías"
"##advchief_some_need_baths##":"Algunos ciudadanos necesitan casas de baños"
"##advchief_some_need_doctors##":"Algunos ciudadanos necesitan consultas"
"##advchief_some_need_education##":"Algunos ciudadanos solicitan más educación."
"##advchief_some_need_hospital##":"Algunos ciudadanos necesitan hospitales"
"##advchief_some_need_library##":"Algunos ciudadanos desean más bibliotecas"
"##advchief_which_crime_in_district##":"Algunas zonas presentan problemas menores"
"##advchief_which_crime##":"La criminalidad se está convirtiendo en un problema."
"##advchief_workless##":"La ciudad tiene un desempleo de"
"##adve_administration_religion##":"Gobernación/religión"
"##adve_engineers##":"Ingeniería"
"##adve_entertainment##":"Entretenimiento"
"##adve_food##":"Producción de alimentos"
"##adve_health_education##":"Sanidad y educación"
"##adve_industry_and_trade##":"Industria y comercio"
"##adve_military##":"Ejército"
"##adve_prefectures##":"Prefecturas"
"##adve_water##":"Servicios hidráulicos"
"##advemp_emperor_favour##":"Favor imperial"
"##advemployer_panel_denaries##":"dn"
"##advemployer_panel_haveworkers##":""
"##advemployer_panel_needworkers##":""
"##advemployer_panel_priority##":""
"##advemployer_panel_romepay##":"Roma paga"
"##advemployer_panel_salary##":"Salarios"
"##advemployer_panel_sector##":""
"##advemployer_panel_title##":"Distribución laboral"
"##advemployer_panel_workers##":""
"##advemployer_panel_workless##":"Mano de obra desempleada "
"##advice_at_culture##":"Haz clic aquí para asesorarte sobre la puntuación de cultura"
"##advisors##":"Asesores"
"##advlegion_noalarm##":"No tenemos informes de que existan amenazas para la ciudad."
"##advlegion_norequest##":"No hemos recibido peticiones de ayuda en ninguna parte del imperio."
"##advlegion_window_title##":"Estado de la Legión"
"##advpopulation_text_census##":"Composición de la población por edades (en años)."
"##advpopulation_text_society##":"Composición de la población por ingresos."
"##advpopulation_title_census##":"Población - Censo"
"##advpopulation_title_population##":"Población - Historia"
"##advpopulation_title_society##":"Población - Sociedad"
"##aedile##":"Edil"
"##age_ad##":"DC"
"##age_bc##":""
"##also_fountain_in_well_area##":"Este pozo es inservible puesto que todas las casas circundantes obtienen su agua de una fuente local."
"##amphitheater_full_work##":"Este anfiteatro ofrece a la comunidad emocionantes combates de gladiadores y representaciones dramáticas de actores locales."
"##amphitheater_have_never_show##":"Este anfiteatro nunca tiene funciones. Se necesitan actores y gladiadores, y no los hay."
"##amphitheater_have_only_battles##":"Este anfiteatro cuenta con luchas de gladiadores para el deleite de la comunidad. Busca actores para organizar algunas representaciones."
"##amphitheater_have_only_shows##":"Este anfiteatro representa obras con actores locales. Se podría agradar más al público si se contara con gladiadores para combates en vivo."
"##amphitheater_haveno_gladiator_bouts##":"No hay combate de gladiadores"
"##amphitheater_haveno_shows##":"No hay representación"
"##amphitheater_no_access##":"Esta casa no tiene acceso a un anfiteatro."
"##amphitheater_no_workers##":"Este anfiteatro está muerto. Sin trabajadores, no ofrece ningún servicio a la comunidad local."
"##amphitheater##":"Anfiteatro"
"##amphitheatres##":"Anfiteatros"
"##angry##":"enfadado"
"##animal_contests_run##":"Cambio de lucha de animales"
"##aqueduct_info##":"Los acueductos permiten la construcción de un depósito lejos del agua, permitiendo así que las fuentes distribuyan agua potable en las zonas internas de la ciudad."
"##aqueduct_no_water##":"Este acueducto no transporta agua entre depósitos ya que no tiene una fuente de donde sacarla."
"##aqueduct_work##":"Este acueducto transporta agua entre depósitos."
"##aqueduct##":"Acueducto"
"##arabian_stallions##":"Pura sangres árabes"
"##architect_salary##":"Salario de arquitecto de"
"##architect##":"Arquitecto"
"##army_marker##":"Marcador de ejército"
"##arrow##":"Flecha"
"##avesome_amphitheater_access##":"Esta casa ha sido visitada recientemente por un actor o un gladiador. Tendrá acceso a un anfiteatro durante mucho tiempo."
"##avesome_clinic_access##":"Esta casa ha sido visitada recientemente por un médico. Tendrá acceso a una consulta durante mucho tiempo."
"##avesome_college_access##":"Esta casa ha sido visitada recientemente por un profesor. Tendrá acceso a la academia durante mucho tiempo."
"##avesome_colloseum_access##":"Por esta casa pasó recientemente un domador de leones o un gladiador. Tendrá acceso a un coliseo durante mucho tiempo."
"##avesome_hippodrome_access##":"Esta casa ha sido visitada recientemente por un auriga. Tendrá acceso a un hipódromo durante mucho tiempo."
"##avesome_hospital_access##":"Esta casa ha sido visitada recientemente por un cirujano. Tendrá acceso a un hospital durante mucho tiempo."
"##avesome_library_access##":"Esta casa ha sido visitada recientemente por un bibliotecario. Tendrá acceso a la biblioteca durante mucho tiempo."
"##avesome_school_access##":"Esta casa ha sido visitada recientemente por un alumno. Tendrá acceso al colegio durante mucho tiempo."
"##avesome_theater_access##":"Esta casa ha sido visitada recientemente por un actor. Tendrá acceso a un teatro durante mucho tiempo."
"##awesome_barber_access##":"Esta casa ha sido visitada recientemente por un barbero. Tendrá acceso a la barbería durante mucho tiempo."
"##awesome_baths_access##":"Esta casa ha sido visitada recientemente por un empleado de baños. Tendrá acceso a la casa de baños durante mucho tiempo."
"##balance##":"Balance"
"##ballista##":"Ballesta"
"##barbarian_warrior##":"Guerrero bárbaro"
"##barber_access##":"Acceso a la barbería"
"##barber_average_life##":"¡La ciudad tiene un corte perfecto!"
"##barber_full_work##":"Esta barbería está en funcionamiento. La comunidad está bien arreglada."
"##barber_gods_angry##":"Los dioses están enfadados. ¡Ojalá el gobernador construyera más templos!"
"##barber_good_life##":"¿Afeitar o cortar? ¿Ciudadano? La ciudad marcha ¿eh?"
"##barber_high_workless##":"¡Hay tanto paro que se me ponen los pelos de punta de sólo pensarlo!"
"##barber_info##":"¡Ningún hombre civilizado aparecería en público sin afeitar! Todos los ciudadanos necesitan cortarse el pelo regularmente si pretenden escalar socialmente."
"##barber_need_colloseum##":"Tras pasarme todo el día afeitando y cortando el pelo, me apetece ver una lucha de leones ¡Aquí no puedo!"
"##barber_need_workers##":"Faltan trabajadores."
"##barber_no_access##":"Esta casa no tiene acceso a una barbería."
"##barber_no_workers##":"Esta barbería no está en funcionamiento, y no sirve a la comunidad local."
"##barber_shop##":"Barbería"
"##barber_so_hungry##":"Irte a cortar el pelo hace que te olvides del hambre. Y hay demasiada hambre en la ciudad."
"##barber##":"Barbería"
"##barbers##":"Barberías"
"##barracks_full_work##":"Estamos entrenando soldados nuevos con el máximo nivel de eficiencia y tenemos las armas para adiestrar a cualquier tipo de soldado."
"##barracks_info##":"Nadie puede incorporarse a las legiones romanas sin antes pasar por aquí; esto es la escuela de entrenamiento de la ciudad para nuevos reclutas."
"##barracks_need_some_workers##":"Estamos entrenando soldados nuevos con un nivel de eficiencia reducido debido a la falta de personal, aunque tenemos las armas para adiestrar a cualquier tipo de soldado."
"##barracks##":"Barracones"
"##bath_1##":"Casa de baños"
"##bath_access##":"Acceso a los baños"
"##bath_no_access##":"Esta casa no tiene acceso a una casa de baños en funcionamiento."
"##bath##":"Casa de baños"
"##bathlady_so_hungry##":"¡La gente lleva tanto sin comer que se les notan las costillas cuando van a los baños!"
"##bathlady##":"Empleado de baños"
"##baths_full_work##":"Esta casa de baños está en funcionamiento. La comunidad está limpia y relajada."
"##baths_info##":"La gente civilizada se baña al menos una vez al día. Además de mejorar la salud pública, los baños son un centro de atracción pues ofrecen a la comunidad varias diversiones relajantes."
"##baths_need_reservoir##":"Esta casa de baños necesita acceso por tuberías a un depósito cercano."
"##baths_no_workers##":"Esta casa de baños no está en funcionamiento.¿Quién se querría bañar aquí sin empleados que les atiendan?"
"##baths##":"Casas de baños"
"##bc##":"AC"
"##become_trade_center##":"Convertir en centro de comercio"
"##below_average##":"Inferior a la media"
"##better_class_road##":"Un mejor tipo de carretera"
"##big_domus##":"Apartamentos grandes"
"##big_hut##":"Casa grande"
"##big_palace##":"Palacio grande"
"##big_tent##":"Tienda grande"
"##big_villa##":"Villa grande"
"##bldm_factory##":"Talleres"
"##bldm_farm##":"Granjas"
"##bldm_raw_materials##":"Materias primas"
"##bldm_raw##":"Fábrica"
"##bolt##":"Proyectil"
"##bridge##":"Puente"
"##bridges##":"Puentes"
"##briton##":"Bretón"
"##britons##":"Bretones"
"##btn_showprice_tooltip##":"Muestra los precios de importación y exportación determinados en Roma"
"##build_fishing_boat##":"Estamos construyendo un barco para una dársena pesquera de la ciudad."
"##build_housing##":"Construir viviendas"
"##build_markets_to_distribute_food##":"Construye mercados para distribuir la comida almacenada aquí."
"##build_road_tlp##":"Construir carreteras"
"##building_need_road_access##":""
"##buildings##":"Construcción"
"##burning_ruins##":"Ruina en llamas"
"##buy_price##":"Los compradores pagan"
"##caesar_assign_new_title##":"El César te ha ascendido al rango de"
"##caesar_salary##":"Salario de César de"
"##can_build_only_one_of_building##":"Solamente puedes tener un edificio de este tipo"
"##cancel##":"Cancelar"
"##cancelBtnTooltip##":"Cancelar esta operación"
"##cant_demolish_bridge_with_people##":"Hay gente en el puente. No puedes derribarlo."
"##captured_city##":"Ciudad capturada"
"##carthaginian_soldier##":"Soldado cartaginense"
"##cartPusher_average_life##":"No es nada divertido empujar un carro todo el día, pero merece la pena por estar en esta ciudad"
"##cartPusher_cantfind_destination##":"¡Acabaría antes llevando esto a Roma que a su verdadero destino!"
"##cartPusher_gods_angry##":"No soy demasiado religioso, pero ni yo me atrevería a tratar tan mal a los dioses."
"##cartPusher_good_life##":"Tal y como están las cosas, la ciudad va bien."
"##cartPusher_high_workless##":"¡El paro es tan elevado que hasta mi mujer ha dejado de repetirme que me busque un empleo mejor!"
"##cartPusher_low_entertainment##":"No me importa empujar un carro. ¡La vida en la ciudad resulta aún más aburrida!"
"##cartPusher_need_workers##":"¡Mire a donde mire sólo veo ofertas de trabajo!"
"##cartPusher_so_hungry##":"Me paso el día empujando un carro ¿cómo voy a seguir adelante sin tener nada que llevarme a la boca?"
"##ceres_badmood_info##":"El descontento de Ceres es peligroso puesto que ella protege de las malas cosechas y del hambre."
"##ceres_desc##":"Agricultura"
"##ceres_goodmood_info##":"Ceres otorga fertilidad a la tierra, y hace crecer las cosechas. Apacíguala o prepárate a pasar hambre."
"##charioteer_school_full_work##":"Nos satisface comunicar que con tanto personal estamos terminando hasta cuatro cuadrigas nuevas cada mes."
"##charioteer_school_need_some_workers##":"Estamos algo escasos de personal, por lo que no podemos terminar más de dos cuadrigas al mes."
"##charioteer_school_patrly_workers##":"Sólo tenemos a la mitad de personal, con lo que sólo podremos terminar una cuadriga este mes."
"##charioteer##":"Auriga"
"##charioter_so_hungry##":"¿Hambre? ¡Me comería un caballo! ¡Apenas hay comida!"
"##chatioteer_school_bad_work##":"Soy el único personal laboral aquí. No puedo trabajar en estas condiciones. En el mejor de los casos, puedo terminar una cuadriga en tres meses."
"##chatioteer_school_bad_work##":"Necesitamos personal urgentemente. Intentaremos terminar una cuadriga en los próximos dos meses."
"##chatioteer_school_no_workers##":"Sin artesanos no se harán nuevas cuadrigas. El hipódromo, de estar operativo, podría sufrir las consecuencias."
"##chatioteer_school##":"Taller de cuadrigas"
"##chief_advisor##":"Asesor jefe"
"##children##":"Niños"
"##citizen_are_rioting##":"¡Los ciudadanos están amotinándose!"
"##citizen_gods_angry##":"¡Esta es una ciudad pagana! ¡Necesitamos más templos!"
"##citizen_gods_angry3##":"El gobernador no respeta a los dioses."
"##citizen_good_education##":"¡Hay más cultura en esta ciudad que en ninguna otra del Imperio!"
"##citizen_high_workless10##":"Hay tanto desempleo que los ciudadanos no son felices."
"##citizen_high_workless2##":"Si no encuentro trabajo pronto, tendré que mudarme a otra ciudad."
"##citizen_high_workless4##":"Nunca me había encontrado con tantos parados."
"##citizen_low_entertainment4##":"¡No hay ningún sitio a donde ir por la noche!"
"##citizen_low_salary##":"Ni mi perro trabajaría por ese sueldo. ¡Me voy!"
"##citizen_need_workers3##":"La ciudad anda escasa de trabajadores."
"##citizen_need_workers5##":"¡La ciudad necesita más trabajadores!"
"##citizen_salary##":"Salario de ciudadano de"
"##citizen##":"Ciudadano"
"##citizens_additional_rooms_for##":"Espacio extra para"
"##city_cyrene##":"Cirene"
"##city_damascus##":"Damasco"
"##city_fire_text##":""
"##city_fire_title##":""
"##city_has_debt##":"La ciudad tiene una deuda con Roma de"
"##city_has_runout_debt##":""
"##city_have_goods_for_request##":""
"##city_have##":"El tesoro de la ciudad tiene un activo de"
"##city_health##":"Sanidad de la ciudad"
"##city_need_more_workers##":"La ciudad necesita más trabajadores."
"##city_sounds_off##":"Sonidos ciudad desactivados"
"##city_sounds_on##":"Sonidos de ciudad activados"
"##city##":"Ciudad"
"##citychart_census##":"Censo"
"##citychart_population##":"Historia"
"##citychart_society##":"Sociedad"
"##clay_factory_stock##":"Arcilla almacenada,"
"##clay_pit_bad_work##":"Con tan poco personal en este pozo de arcilla, la producción es casi inexistente. Producirá muy poca arcilla el próximo año."
"##clay_pit_full_work##":"Este pozo de arcilla tiene todo el personal que necesita. Se esmera en producir arcilla."
"##clay_pit_info##":"Se crea arcilla para comerciar o para abastecer a los talleres de cerámica. Los plebeyos necesitan cerámica. Además, podrás comerciar cerámica con otras provincias."
"##clay_pit_need_close_to_water##":"Construye pozos de arcilla cerca del agua."
"##clay_pit_need_some_workers##":"Este pozo de arcilla está trabajando por debajo de su capacidad máxima. Como resultado, la producción de arcilla será algo inferior."
"##clay_pit_no_workers##":"Este pozo de arcilla no tiene empleados. La producción se ha detenido."
"##clay_pit_patrly_workers##":"Este pozo de arcilla tiene muy poco personal, por lo que la producción de arcilla es más lenta de lo deseado."
"##clay_pit_patrly_workers##":"Esta cantera tiene muy poco personal, por lo que la extracción del mármol es más lenta de lo deseado."
"##clay_pit##":"Pozo de arcilla"
"##clay##":"Arcilla"
"##clear_land_caption##":"Tierra vacía"
"##clear_land_text##":"En esta tierra se puede edificar libremente y por ella pueden pasar no sólo tropas aliadas, sino también enemigas."
"##clear_land_text##":""
"##clear_land##":"Despejar tierra"
"##clearBtnTooltip##":""
"##clerk_salary##":"Salario de empleado de"
"##clerk##":"Empleado"
"##click_here_that_stacking##":"Haz clic aquí para reservarlo"
"##click_here_that_use_it##":"Haz clic aquí para desactivar la reserva"
"##click_item_for_start_trade##":"Haz clic en un objeto para comerciar."
"##clinic_full_work##":"Esta consulta está en funcionamiento y ofrece sus servicios a la comunidad."
"##clinic_info##":"Los médicos mejoran la salud de los ciudadanos puesto que hacen visitas domésticas en las cercanías. Las zonas más ricas quieren una consulta."
"##clinic_no_workers##":"Esta consulta no está en funcionamiento, y no sirve a la comunidad local."
"##clinic##":"Médico"
"##clinics##":"Consultas"
"##collapse_immitent##":"Derrumbamiento inminente"
"##collapsed_ruins_info##":"Estos escombros de edificios antiguos rebajan el atractivo de la tierra en sus alrededores."
"##colloseum_haveno_animal_bouts##":"No hay luchas con animales"
"##colloseum_haveno_gladiator_bouts##":"No hay combate de gladiadores"
"##colloseum_haveno_gladiatorpit##":"Construye una escuela de gladiadores para organizar luchas."
"##colloseum_info##":"Los coliseos y anfiteatros siempre necesitan nuevos gladiadores para sustituir a los perdedores."
"##colloseum_no_workers##":"Este coliseo está cerrado. Sin empleados que se encarguen de él, no ofrece diversión a la comunidad."
"##colloseum##":"Coliseo"
"##colloseums##":"Coliseos"
"##colosseum_no_access##":"Esta casa no tiene acceso a un coliseo."
"##column_info##":"Formación en columna"
"##comerceBtnTooltip##":""
"##commerce##":"Comercio"
"##congratulations##":"Enhorabuena"
"##consul_salary##":"Salario de cónsul de"
"##consul##":"Cónsul"
"##contaminted_water##":"Agua contaminada"
"##continue##":""
"##corinthus##":"Corinto"
"##cost_2_open##":"Coste de apertura"
"##cost##":"Coste"
"##costs##":"costes"
"##coverage##":"Cobertura en la ciudad"
"##crack##":"Grieta"
"##credit##":"Gastos"
"##cursed_by_mars##":"¡Maldito por Marte!"
"##damage##":"Daño"
"##date_tooltip##":""
"##date##":"Fecha"
"##day_longer_in_that_tent##":"Un día más en esa tienda y me habría desquiciado."
"##day##":"Día"
"##days##":"Días"
"##debet##":"Beneficio"
"##dedicate_fectival_ceres##":"Dedicar un festival a Ceres"
"##dedicate_fectival_mars##":"Dedicar un festival a Marte"
"##dedicate_fectival_mercury##":"Dedicar un festival a Mercurio"
"##dedicate_fectival_neptune##":"Dedica un festival a Neptuno"
"##dedicate_fectival_venus##":"Dedicar un festival a Venus"
"##defensive_formation_text##":"Formación muy defensiva. Virtualmente inmune a los proyectiles."
"##delete_game##":""
"##delete_object##":"Borrar objeto"
"##delete_this_message##":"Borrar este mensaje"
"##delighted##":"encantado"
"##deliver##":"Consigue bienes"
"##delivery_boy##":"Chico de reparto"
"##demand##":"Demanda"
"##demands_3_religion##":"Demandas para acceso a una tercera religión"
"##denarii_short##":""
"##desirability##":"Atractivo"
"##destroy_bridge_warning##":"Por favor vuelve a colocar el CD de Caesar III en la unidad de CD-ROM"
"##destroy_bridge##":"CD ausente"
"##destroy_fort##":"Demolición de un fuerte"
"##devastate_granary##":"Vaciar granero"
"##difficulty##":""
"##disasterBtnTooltip##":""
"##dispatch_force##":"¿Enviar fuerzas de refresco?"
"##dispatch_gift##":"Mandar un regalo."
"##dispatch_git_title##":"Enviar regalo al emperador"
"##dispatch_goods?##":"¿Despachar bienes?"
"##distant_city##":"Una ciudad lejana."
"##distribution_center##":"Centro de distribución"
"##dn_collected_this_year##":"denarios recaudados hasta ahora en este año."
"##dn_for_open_trade##":"para abrir la ruta comercial."
"##dn_per_month##":"Denarios al mes"
"##dn's##":"Salir editor de mapas"
"##dn##":"Guardar mapa"
"##dock_cart_returning_from":"Nuestro carro vuelve de hacer un reparto."
"##dock_cart_taking_goods##":"Nuestro carro lleva bienes a otras partes."
"##dock_cart_wait##":"Nuestro carro está aquí, esperando nuevas instrucciones."
"##dock_full_work##":"Con tanto personal, los estibadores simplemente esperan a que entre un barco."
"##dock_info##":"Los estibadores están en el almacén, recogiendo unas mercancías que hemos comprado."
"##dock_info##":"Los barcos de mercancías de todo el imperio atracan aquí para dejar importaciones y cargar exportaciones. No puedes viajar por mar sin muelles."
"##dock_need_some_workers##":"Tenemos poco personal, por lo que tardaremos más de lo deseado en cargar y descargar la mercancía de los barcos que lleguen."
"##dock_no_workers##":"El barco que atraque aquí no dispondrá de trabajadores para la carga y descarga de mercancías."
"##dock_no_workers##":"Al no tener estibadores en plantilla no podemos ofrecer servicios al barco atracado."
"##dock##":"Muelle"
"##docked_buying_selling_goods##":"En el muelle, comprando y vendiendo bienes."
"##doctor_average_life##":"Es una ciudad maravillosa."
"##doctor_gods_angry##":"Los dioses están enfadados. Si no les demostramos más respeto, sufriremos su ira."
"##doctor_good_life##":"La ciudad parece estar sana."
"##doctor_high_workless##":"Hay demasiado desempleo. Estoy pensando en marcharme de aquí."
"##doctor_low_entertainment##":"¡La ciudad es tan aburrida que los pacientes me piden que les cure el aburrimiento crónico!"
"##doctor_need_workers##":"!Saludos! Aqui hay verdadera escasez de trabajadores."
"##doctor_no_access##":"Esta casa no tiene acceso a una consulta."
"##doctor_so_hungry##":"La gente está desnutrida, ¡y no hay comida!"
"##donations##":"Donaciones"
"##east##":"Este"
"##edil_salary##":"Salario de edil de"
"##education_advisor_title##":"Educación"
"##education_have_academy_access##":"Esta casa tiene acceso a una academia."
"##education_have_no_access##":"Esta casa no tiene acceso a ningún colegio o biblioteca."
"##education_have_school_library_access##":"Esta casa tiene acceso a un colegio y a una biblioteca."
"##education_have_school_or_library_access##":"Esta casa tiene acceso a un colegio o a una biblioteca."
"##education_objects##":"Estructuras educativas"
"##education##":"Educación"
"##educationBtnTooltip##":""
"##egift_chest_of_sapphire##":"Un cofre de zafiros"
"##egift_educated_slave##":"Un esclavo educado"
"##egift_egyptian_glassware##":"Vajilla egipcia"
"##egift_gaulish_bodyguards##":"Guardias personales galos"
"##egift_generous##":"Generoso:"
"##egift_gepards_and_giraffes##":"Guepardos y una jirafa"
"##egift_golden_chariot##":"Un carro de oro"
"##egift_gree_manuscript##":"Un manuscrito griego"
"##egift_lavish##":"Lujoso:"
"##egift_modest##":"Modesto:"
"##egift_persian_carpets##":"Alfombras persas"
"##egift_soldier_from_pergamaum##":"Soldado de Pérgamo"
"##egift_troupe_preforming_slaves##":"Una tropa de esclavos"
"##emigrant_high_workless##":"¿Sabes si hay trabajo? ¡Lo necesito!"
"##emigrant_no_home##":"¡No tengo dónde vivir!"
"##emigrant_no_work_for_me##":"¡Ya me he cansado de la ciudad! ¡No hay trabajo!"
"##emigrant_thrown_from_house##":"¡Me han echado de mi propia casa!"
"##emigrant##":"Emigrante"
"##empbutton_tooltip##":"Haz clic aquí para establecer una prioridad para esta categoría laboral."
"##emperor_favour_00##":"El emperador está furioso contigo."
"##emperor_favour_01##":"El emperador está tan terriblemente enfadado que incluso ha pensado mandarte al exilio."
"##emperor_favour_02##":"El emperador está extremadamente enfadado contigo."
"##emperor_favour_03##":"El emperador está muy enfadado contigo."
"##emperor_favour_04##":"El emperador está enfadado contigo."
"##emperor_favour_05##":"El emperador está extremadamente decepcionado contigo."
"##emperor_favour_06##":"El emperador está totalmente decepcionado contigo."
"##emperor_favour_07##":"El emperador está decepcionado contigo."
"##emperor_favour_08##":"El emperador está algo decepcionado contigo."
"##emperor_favour_09##":"El emperador está un poco preocupado contigo."
"##emperor_favour_10##":"El emperador se muestra contradictorio contigo."
"##emperor_favour_11##":"El emperador piensa que puedes hacerlo bien."
"##emperor_favour_12##":"El emperador está satisfecho contigo."
"##emperor_favour_13##":"El emperador se siente muy agradado por tu actuación."
"##emperor_favour_14##":"El emperador está muy entusiasmado con tu actuación."
"##emperor_favour_15##":"El emperador se siente agradado por tu actuación."
"##emperor_favour_16##":"El emperador está entusiasmado con tu actuación."
"##emperor_favour_17##":"El emperador está extremadamente entusiasmado con tu actuación."
"##emperor_favour_18##":"El emperador está encantado con tu actuación."
"##emperor_favour_19##":"El emperador está más que satisfecho con tu actuación."
"##emperor_favour_20##":"El emperador está increíblemente encantado; hasta piensa nombrarte su heredero."
"##emperor_request##":"Solicitud imperial"
"##emperor##":"Emperador"
"##emperror_legion_at_out_gates##":"Una legión de tropas del emperador se encuentra a nuestras puertas."
"##empire_map##":"Ir al Imperio"
"##empire_tax##":"Tributos"
"##empireBtnTooltip##":""
"##employee##":"unidad"
"##employees##":"unidades"
"##employers##":"Mano de obra empleada"
"##emw_bought##":"Comprado"
"##emw_buy##":"Compra"
"##emw_sell##":"Ventas"
"##enemy_attack_city##":""
"##engineer_average_life##":"Todo parece ir bien."
"##engineer_building_allok##":"Aquí no hago falta. Los edificios están en perfecto estado."
"##engineer_gods_angry##":"¡Que los dioses se apiaden de mi! ¡No tengo la culpa de que el gobernador se burle de ellos!"
"##engineer_good_life##":"¡Salve! ¿No es una gran ciudad?"
"##engineer_have_trouble_buildings##":"Estos edificios estaban en mal estado. Menos mal que he llegado a tiempo."
"##engineer_high_workless##":"Con tanto desempleo, es una suerte tener trabajo."
"##engineer_low_entertainment##":"Tras un día de duro trabajo, me apetece ver una representación o unos combates, pero no puedo hacerlo en esta ciudad."
"##engineer_need_workers##":"La ciudad iría mejor si hubiera más trabajadores."
"##engineer_salary##":"Salario de ingeniero de"
"##engineer_so_hungry##":"Si no consigo comida pronto, me iré de la ciudad."
"##engineer##":"Ingeniero"
"##engineerBtnTooltip##":""
"##engineering_post_bad_work##":"Estamos trabajando con personal ficticio. Apenas podemos contar con un ingeniero cada mes."
"##engineering_post_full_work##":"Actualmente no tenemos tiempo libre. Nuestros ingenieros siempre están en la calle, examinando y reparando los edificios de la ciudad."
"##engineering_post_info##":"Los ingenieros son profesionales muy reconocidos cuyos servicios siempre están solicitados. Su mantenimiento continuo previene el derrumbamiento de los edificios."
"##engineering_post_need_some_workers##":"Falta un par de días para que nuestros ingenieros saturados de trabajo vuelvan a las calles."
"##engineering_post_no_workers##":"Sin ingenieros, hasta este despacho corre el riesgo de desmoronarse."
"##engineering_post_on_patrol##":"Nuestro ingeniero está trabajando."
"##engineering_post_patrly_workers##":"Contamos con muy poco personal, por lo que tendremos que esperar una semana antes de volver a disponer de los ingenieros."
"##engineering_post_ready_for_work##":"Nuestro ingeniero se prepara para irse."
"##engineering_post_slow_work##":"Estamos tan escasos de personal, que entre una ronda de ingenieros y la siguiente pasan dos semanas."
"##engineering_post##":"Puesto de ingenieros"
"##engineering_structures##":"Construcciones de ingeniería"
"##enter_your_name##":"Escoger un nombre"
"##entertainment_advisor_title##":"Entretenimiento"
"##entertainment_short##":"Entr."
"##entertainment##":"Entretenimiento"
"##entertainmentBtnTooltip##":""
"##exit_point##":"Exit point"
"##exit_salary_window##":"Salir de la pantalla de salario"
"##exit_this_panel##":"Salir de este panel"
"##exit_without_saving?##":"Ten cuidado al derribar puentes. Las comunidades aisladas se extinguen cuando se les corta la comunicación por carretera con Roma."
"##exit##":"Salir"
"##explosion##":"Explosión"
"##export_btn_tooltip##":"Establecer la cantidad del producto que deseas conservar antes de exportarlo"
"##exports_over##":"Exportar por encima de"
"##extreme_fire_risk##":"Riesgo de incendio extremo"
"##factory_need_more_workers##":"Apenas funciona. Destina más trabajadores a nuestro sector."
"##farm_need_farmland##":"Construye granjas en tierra de cultivo (busca hierba amarilla)."
"##farm_working_bad##":""
"##farm_working_normally##":""
"##farm##":"Granjas"
"##favor_rating##":"Puntos de favor"
"##favor##":"Favor"
"##festivals##":"Festivales"
"##fig_farm_bad_work##":"Aquí trabajan muy pocos granjeros. La cosecha de fruta se retrasará mucho."
"##fig_farm_full_work##":"Este huerto tiene todo el personal que necesita. Los árboles rebosan de jugosa fruta."
"##fig_farm_info##":"La fruta es importante en la dieta equilibrada que tu gente necesita para estar sana y feliz. Los graneros almacenan fruta, y los almacenes conservan el excedente para la exportación."
"##fig_farm_need_some_workers##":"Este huerto está falto de personal. Está produciendo fruta más lentamente de lo que podría."
"##fig_farm_no_workers##":"Este huerto no tiene empleados. La producción se ha detenido."
"##fig_farm_patrly_workers##":"Este huerto está trabajando por debajo de su capacidad máxima. Como resultado, la producción de fruta será algo más lenta."
"##fig_farm##":"Granja de frutas"
"##file##":"Archivo"
"##finance_advisor##":"Fondos actuales de la ciudad"
"##finance_advisor##":"Economía"
"##finances##":"Economía"
"##fire##":"Fuego"
"##fired##":"¡Despedido!"
"##fishing_boat##":"Barco de pesca"
"##fishing_waters##":""
"##fishing_wharf##":"Dársena pesquera"
"##floatsam_enabled##":"Flotsam on?"
"##floatsam##":"Restos flotantes"
"##fort_horse##":"Tropas auxiliares montadas"
"##fort_info##":"Los fuertes reclutan soldados procedentes de los barracones de la ciudad. La creación de una academia militar suministrará tropas mejor entrenadas."
"##fort_javelin##":"Jabalina"
"##fort_legionaries_no_workers##":"Sin personal somos incapaces de entrenar a un solo nuevo recluta. ¡Que Marte nos ayude en caso de guerra!."
"##fort_legionaries##":"Fuerte de legionarios"
"##fort##":"Fuerte"
"##fortification_info##":"Las murallas evitan el avance de los invasores hacia la ciudad. Éstos pueden destruir las murallas. Las murallas gruesas son más fuertes y permiten patrullar a los guardias entre las torres conectadas."
"##fortification##":""
"##forum_1_on_patrol##":"Nuestro recaudador está inspeccionando."
"##forum_1_ready_for_work##":"Nuestro recaudador se prepara para irse."
"##forum_1_slow_work##":"Estamos tan escasos de personal, que entre una ronda de recaudación y la siguiente, pasan dos semanas."
"##forum_full_work##":"Actualmente trabajamos con total eficacia. Nuestros recaudadores siempre están en la calle controlando el pago de los tributos que se deben."
"##forum_information##":"Un popular lugar de reunión y un elemento atractivo para la comunidad. Además, los foros hacen uso de recaudadores y son vitales para la economía de la ciudad."
"##forum_need_some_workers##":"Falta un par de días para que nuestros recaudadores saturados de trabajo vuelvan a las calles."
"##forum_no_workers##":"Sin recaudadores, este despacho no contribuye en absoluto a engrosar las arcas de la ciudad."
"##forum_patrly_workers##":"Contamos con muy poco personal, por lo que tendremos que esperar una semana antes de volver a disponer de los recaudadores."
"##forum##":"Foro"
"##fountain_info##":"La gente extrae el agua gratuitamente de las fuentes, que deben recibir su suministro de un depósito. Las fuentes requieren la contratación de más trabajadores del servicio hidráulico. Todo el mundo prefiere el agua de las fuentes a la de otros suministros."
"##fountain_not_work##":"Esta fuente no funciona. Tu asesor laboral debería destinar más trabajadores a los servicios hidráulicos."
"##fountain##":"Fuente"
"##free##":"Gratis"
"##freehouse_caption##":"Parcela libre"
"##freehouse_text_noroad##":"Nadie levantará su casa aquí, ya que está demasiado lejos de la carretera. A menos que haya acceso por carretera pronto, este lugar volverá a ser campo abierto."
"##freehouse_text##":"Nadie ha puesto sus pies aquí, aunque es probable que lleguen inmigrantes si la ciudad les ofrece comida y trabajo."
"##freespace_for##":"Sitio para"
"##fruit_farm_slow_work##":"Con tan pocos granjeros, el cultivo de fruta aquí tardará mucho tiempo."
"##fruit##":"Frutas"
"##fullscreen_off##":"Pantalla con ventanas"
"##fullscreen_on##":"Pantalla completa"
"##funds_tooltip##":""
"##furniture_need##":"Muebles necesarios"
"##furniture_workshop_bad_work##":"Con tan poco personal en este taller, la producción es casi inexistente. Producirá muy pocos muebles el próximo año."
"##furniture_workshop_full_work##":"Este taller tiene todo el personal que necesita. Se esmera en producir muebles."
"##furniture_workshop_info##":"Los carpinteros tallan finos muebles de madera. Algunos ciudadanos quieren muebles para sus hogares pero también puedes comerciar con el excedente."
"##furniture_workshop_need_resource##":"Este taller necesita recibir madera de un almacén o de una serrería para producir muebles."
"##furniture_workshop_need_some_workers##":"Este taller tiene muy poco personal, por lo que la producción de muebles es más lenta de lo deseado."
"##furniture_workshop_patrly_workers##":"Este taller tiene algunas plazas libres. Cuando sean ocupadas se mejorará la producción de muebles."
"##furniture_workshop_slow_work##":"Aquí trabajan muy pocos carpinteros. Como resultado, la producción de muebles es muy lenta."
"##furniture_workshop##":"Taller de carpintería"
"##furniture##":"Muebles"
"##game_is_paused##":"Juego en pausa (Pulsar 'P' para continuar)"
"##game_sound_options##":""
"##game_speed_options##":""
"##garden_info##":"Este lugar permite escapar del ruido, el calor y la suciedad de la ciudad ofreciendo a los ciudadanos la frescura de un verde oasis. Todo el mundo quiere un jardín junto a su casa."
"##garden##":"Jardines"
"##gardens_info##":"Los jardines mejoran el entorno de la zona."
"##gatehouse##":"Portón"
"##give_money##":"Dar dinero"
"##gladiator_gods_angry##":"Salve, ciudadano. ¿Has oído que los dioses están enfadados?"
"##gladiator_good_life##":"¡Saludos! La vida aquí es agradable ¿verdad?"
"##gladiator_high_workless##":"Hay demasiados parados. ¡Me gustaría poder entrenar con ellos!"
"##gladiator_low_entertainment##":"¡Aburrido! ¡Eso dicen de la ciudad, a pesar de mis esfuerzos! Desde luego, la ciudad necesita más espectáculos."
"##gladiator_need_workers##":"Es espantoso. Nunca había visto tantos puestos de trabajo vacantes."
"##gladiator_perfect_life##":"¡Ciudadano! He luchado en un montón de ciudades y esta es, sin duda, la mejor."
"##gladiator_pit_bad_work##":"Soy el único personal laboral aquí. No puedo trabajar en estas condiciones. En el mejor de los casos, puedo entrenar a un gladiador en tres meses."
"##gladiator_pit_full_work##":"Nos satisface comunicar que con tanto personal estamos entrenando hasta a cuatro gladiadores nuevos cada mes."
"##gladiator_pit_need_some_workers##":"Estamos algo escasos de personal, por lo que no podemos entrenar a más de dos gladiadores al mes."
"##gladiator_pit_no_workers##":"Sin entrenadores, esta escuela no podrá formar a nuevos gladiadores."
"##gladiator_pit_patrly_workers##":"Sólo tenemos a la mitad de personal, con lo que sólo podemos entrenar a un gladiador este mes."
"##gladiator_pit_slow_work##":"Necesitamos personal urgentemente. Intentaremos entrenar a un gladiador en los próximos dos meses."
"##gladiator_pit##":"Escuela de gladiadores"
"##gladiator_so_hungry##":"¡Tengo tanta hambre que me comería un león!"
"##gmenu_about##":"Acerca de Caesar 3"
"##gmenu_advisors##":""
"##gmenu_exit_game##":"Salir"
"##gmenu_file_exit##":""
"##gmenu_file_mainmenu##":""
"##gmenu_file_restart##":""
"##gmenu_file_save##":""
"##gmenu_file##":""
"##gmenu_help##":""
"##gmenu_options##":""
"##gmsndwnd_ambient_sound##":"Efectos sonido "
"##gmsndwnd_game_volume##":"Volumen"
"##gmsndwnd_theme_sound##":"Música"
"##gmspdwnd_game_speed##":""
"##gmspdwnd_scroll_speed##":""
"##go_to_problem##":"Haz clic aquí para ir a este punto conflictivo"
"##go2_problem_area##":"Ir a zona conflictiva."
"##god_ceres_short##":""
"##god_charmed##":"muy contento"
"##god_displeased##":"desencantado"
"##god_exalted##":"exaltado"
"##god_excellent##":"Excelente"
"##god_good##":"Buena"
"##god_happy##":"feliz"
"##god_indifferent##":"indiferente"
"##god_irriated##":"irritado"
"##god_mars_short##":""
"##god_mercury_short##":""
"##god_neptune_short##":""
"##god_pleased##":"contento"
"##god_poor##":"Pobre"
"##god_quitepoor##":"Bastante pobres"
"##god_veryangry##":"muy enfadado"
"##god_verygood##":"Muy buena"
"##god_verypoor##":"Muy pobre"
"##god_wrathful##":"colérico"
"##goth_warrior##":"Guerrero godo"
"##goto_empire_map##":"Ir al mapa del Imperio"
"##goto##":"Ir a la"
"##governor_palace_1_info##":"Tu hogar es uno de los lugares más atractivos de la ciudad. Da una idea del nivel de prosperidad que la ciudad puede alcanzar."
"##governor_palace_1##":"Casa del gobernador"
"##governor_palace_2##":"Villa del gobernador"
"##governor_palace_3##":"Palacio del gobernador"
"##governor_salary_title##":"Salario personal"
"##granaries_holds##":"meses"
"##granary_holds##":"mes"
"##granary_info##":"Los graneros llenos son vitales para mantener los estómagos llenos y atraer a nuevos ciudadanos. Un granero puede almacenar grano, carne o pescado, vegetales y fruta."
"##granary_orders##":"Instrucciones del granero"
"##granery##":"Granero"
"##grape_factory_stock##":"Uvas almacenadas,"
"##grape##":"Viñas"
"##grass##":"Césped"
"##greek_soldier##":"Soldado griego"
"##have_food_for##":"Suministros de alimentos para"
"##have_less_academy_in_city_0##":"Tienes muy pocas academias en la ciudad. Construir más mejoraría esta puntuación."
"##have_less_library_in_city_0##":"Tienes muy pocas bibliotecas en la ciudad. Construir más mejoraría esta puntuación."
"##have_less_school_in_city_0##":"Tienes muy pocos colegios en la ciudad. Construir más mejoraría esta puntuación."
"##have_less_temple_in_city_0##":"Tienes muy pocos lugares de culto en la ciudad. Construir más mejoraría esta puntuación."
"##have_less_theater_in_city_0##":"Tienes muy pocos teatros en la ciudad. Construir más mejoraría esta puntuación."
"##have_no_food_on_next_month##":"nada de comida para el próximo mes."
"##have_no_legions##":"No tienes ninguna legión que enviar."
"##have_no_requests##":"Actualmente no tienes mensajes que leer. Cuando la ciudad crezca o el emperador te solicite bienes, los mensajes aparecerán aquí."
"##health_advisor##":"Sanidad de la ciudad"
"##health_advisor##":"Estructuras sanitarias"
"##health##":"Sanidad"
"##healthBtnTooltip##":""
"##help##":"Ayuda"
"##hide_bigpanel##":"Oculta el panel de control para ver una zona de juego más amplia"
"##high_bridge##":"Puente de barcos"
"##hippodrome_full_access##":"Esta casa tiene acceso a un hipódromo."
"##hippodrome_haveno_races##":"No hay carreras"
"##hippodrome_no_access##":"Esta casa no tiene acceso a un hipódromo."
"##hippodrome_no_workers##":"No hay movimiento en el hipódromo. Sin trabajadores, los ciudadanos no pueden disfrutar de sus carreras."
"##hippodrome##":"Hipódromo"
"##hippodromes##":"Hipódromos"
"##hold_ceres_festival##":"Celebrar festival en honor a Ceres"
"##hold_mars_festival##":"Celebrar festival en honor a Marte"
"##hold_mercury_festival##":"Celebrar festival en honor a Mercurio"
"##hold_neptune_festival##":"Celebrar festival en honor a Neptuno"
"##hold_venus_festival##":"Celebrar festival en honor a Venus"
"##hospital_full_access##":"Esta casa tiene acceso a un hospital."
"##hospital_full_work##":"Este hospital está en funcionamiento y ofrece sus servicios a la comunidad."
"##hospital_info##":"Aunque nadie quiere vivir cerca de ellos, los hospitales salvan vidas cuando las enfermedades atacan. La ciudad debería estar dotada de camas suficientes para todos los ciudadanos."
"##hospital_no_access##":"Esta casa no tiene acceso a un hospital."
"##hospital_no_workers##":"Este hospital no está en funcionamiento, y no sirve a la comunidad local."
"##hospital##":"Hospital"
"##hospitals##":"Hospitales"
"##house_evolves_at##":"Esta vivienda pronto se convertirá en una de mayor categoría, como resultado de la mejora de las condiciones locales."
"##house_food_only_for_month##":"Esta casa tiene una reserva de alimentos que durará al menos hasta el mes próximo."
"##house_have_not_food##":"Esta casa no tiene reserva de alimentos."
"##house_have_some_food##":"Esta casa gastará pronto su limitada reserva de alimentos."
"##house_no_troubles_with_food##":"Esta casa no tiene problemas para conseguir la comida necesaria que le permita salir adelante ."
"##house_not_report_about_crimes##":"Los residentes no informan de incidentes violentos."
"##house_provide_food_themselves##":"Los habitantes de las tiendas obtienen su comida del campo cercano."
"##houseBtnTooltip##":""
"##hun_warrior##":"Guerrero huno"
"##immigrant_much_food_here##":"Dicen que hay comida aquí ¿Es un buen sitio para vivir?"
"##immigrant_so_hungry##":"Si me quedo, me moriré. No hay comida por ninguna parte."
"##immigrant_want_to_be_liontamer##":"He oido que hay trabajo ¡Yo quiero ser domador de leones!"
"##immigrant_where_my_home##":"Soy nuevo aquí ¿conoces algún lugar donde pueda vivir?"
"##import_fn##":"Importaciones"
"##import##":"Importación"
"##infobox_tooltip_exit##":""
"##infobox_tooltip_help##":""
"##inland_lake_text##":"Este lago interior no está conectado al mar."
"##iron_factory_stock##":"Hierro almacenado,"
"##iron_mine_bad_work##":"Aquí trabajan muy pocos mineros. Como resultado, la extracción de hierro es lenta."
"##iron_mine_bad_work##":"Con tan pocos mineros, la producción es inexistente. Se extraerá muy poco hierro el próximo año."
"##iron_mine_collapse##":"Derrumbamiento en mina de hierro"
"##iron_mine_full_work##":"Esta mina tiene todo el personal que necesita y se esmera en extraer hierro."
"##iron_mine_info##":"Se extrae hierro para el comercio o para abastecer a los talleres de armas. Equipa a los legionarios con armas de elaboración propia o expórtalas a otras provincias."
"##iron_mine_need_some_workers##":"Esta mina está trabajando por debajo de su capacidad máxima. Con más mineros la extracción del hierro seria más eficaz."
"##iron_mine_no_workers##":"Esta mina no tiene empleados. La producción se ha detenido."
"##iron_mine_patrly_workers##":"Esta mina tiene muy poco personal, por lo que la extracción del hierro es más lenta de lo deseado."
"##iron_mine##":"Mina de hierro"
"##iron##":"Hierro"
"##judaean_warrior##":"Guerrero judío"
"##labor##":"Trabajadores"
"##landmerchant_good_deals##":"¡Me encanta venir aquí! ¡Los negocios van viento en popa!"
"##landmerchant_noany_trade##":"No sé por qué sigo haciendo esta ruta. ¡Nunca me compran nada, ni tienen nada que vender!"
"##landmerchant_say_about_store_goods##":"¡Cuidado! Ojalá estos trabajadores tuvieran más cuidado a la hora de cargar mis animales con mis nuevas adquisiciones."
"##landmerchart_noany_trade2##":"Nada con lo que comerciar, sólamente de paso."
"##large_temple##":"Templo grande"
"##large_temples##":"Templos grandes"
"##large##":"grande"
"##last_year##":"El año pasado"
"##lawless_area##":"Una zona sin ley. Los ciudadanos están aterrorizados."
"##layer_crime##":"Criminalidad"
"##leave_empire?##":"¿Abandonar el Imperio Romano?"
"##left_click_open_right_erase##":"Haz clic izquierdo sobre un mensaje para leerlo. Haz clic derecho para borrarlo."
"##legion_formation_tooltip##":"Haz clic aquí para cambiar la formación de la legión"
"##legion_haveho_soldiers_and_barracks##":"Esta legión no tiene soldados actualmente. Solamente existe sobre el papel, y al no tener barracones operativos en la ciudad, no puede recibir nuevas tropas."
"##legion_haveho_soldiers##":"Esta legión no tiene soldados actualmente. Solamente existe sobre el papel. Se convertirá en una unidad de combate en el momento en que lleguen tropas recién entrenadas de los barracones."
"##legion##":"Legión"
"##legionary_average_life##":"¡Lucharé hasta morir! ¡La ciudad estará a salvo mientras siga vivo!"
"##legionary_low_salary##":"¡No me pagan lo suficiente para luchar!"
"##legions##":"Legiones"
"##lgn_heroes##":"Los Héroes"
"##lgn_hydras##":"Las Hidras"
"##lgn_lions##":"Los Leones"
"##lgn_pigs##":"Los Cerdos"
"##lgn_rabbits##":"Los Conejos"
"##lgn_snakes##":"Las Serpientes"
"##lgn_stallion##":"Los Potros"
"##lgn_wolves##":"Los Lobos"
"##libraries##":"Bibliotecas"
"##library_full_access##":"Esta casa tiene acceso a una biblioteca."
"##library_full_work##":"Esta biblioteca está en funcionamiento. Sus estanterías rebosan sabiduría."
"##library_info##":"Las obras literarias de todo el imperio se almacenan aquí en latín y griego. Los estudiantes insisten en la importancia de las bibliotecas en una ciudad."
"##library_no_access##":"Esta casa no tiene acceso a una biblioteca."
"##library_no_workers##":"Los fondos de esta biblioteca están vacíos y no sirven a la comunidad."
"##library##":"Biblioteca"
"##lion_pit_bad_work##":"Soy el único personal laboral aquí. No puedo trabajar en estas condiciones. En el mejor de los casos, puedo suministrar un león en tres meses."
"##lion_pit_need_some_workers##":"Estamos algo escasos de personal, por lo que no podemos proporcionar más de dos leones por mes."
"##lion_pit_no_workers##":"Sin domadores, este Foso de leones no podrá proporcionar nuevos leones a las luchas."
"##lion_pit_patrly_workers##":"Sólo tenemos a la mitad de personal, con lo que sólo podremos suministrar un león este mes."
"##lion_pit_slow_work##":"Necesitamos personal urgentemente. Apenas podremos suministrar un león en los próximos dos meses."
"##lion_pit_work##":"Nos satisface comunicar que con tanto personal estamos suministrando hasta cuatro leones nuevos al mes."
"##lion_pit##":"Foso de leones"
"##lionTamer_average_life##":"Aquí tienes carne importada Leo."
"##lionTamer_gods_angry##":"Los dioses están tan furiosos, mi león lo nota. ¡Ruge como loco!"
"##lionTamer_good_education##":"¡Salve, ciudadano! ¿Esta ciudad? Lo cierto es que nos gusta, ¿verdad, Leo?"
"##lionTamer_good_life##":"Ahora es tu oportunidad, Leo, ¡buen león!"
"##lionTamer_high_workless##":"Por aquí hay demasiado desempleo."
"##lionTamer_low_entertainment##":"Leo y yo luchamos todo el día y la gente aún se aburre. ¡Hacen falta más espectáculos!"
"##lionTamer_need_workers##":"La ciudad necesita trabajadores. ¿Podré enseñar a Leo a trabajar?"
"##lionTamer_so_hungry##":"Si el león no consique comida, se comerá a su propio domador ¡yo!"
"##Load_save##":"Cargar partida guardada"
"##loading_resources##":"Carga de recursos"
"##low_bridge##":"Puente bajo"
"##low_damage_risk##":"Riesgo de derrumbamiento bajo"
"##low_desirability_degrade##":"Esta casa decaerá pronto. El escaso atractivo de la localidad acabará por arruinar la zona."
"##low_desirability##":"Necesitas mejorar el atractivo de la zona; para ello tal vez puedas crear nuevos jardines o una plaza."
"##low_fire_risk##":"Riesgo de incendio bajo"
"##low_wage_broke_migration##":"Los bajos salarios frenan la inmigración"
"##low_wage_broke_migration##":"Los bajos salarios están limitando la inmigración a la ciudad."
"##low_wage_lack_migration##":"Los salarios bajos son un problema"
"##lumber_mill_bad_work##":"Aquí trabaja muy poco personal. Como resultado, la producción de madera es lenta."
"##lumber_mill_bad_work##":"Con tan pocos taladores, la producción es inexistente. Los bosques vuelven a ser agrestes."
"##lumber_mill_full_work##":"Esta serrería tiene todo el personal que necesita. Se esmera mucho en talar árboles."
"##lumber_mill_info##":"Se corta madera para el comercio o para abastecer a talleres de muebles. Los ciudadanos quieren muebles para sus hogares y además puedes exportarlos a tus socios comerciales."
"##lumber_mill_need_some_workers##":"Esta serrería tiene muy poco personal, por lo que cortar la madera es más lento de lo deseado."
"##lumber_mill_need_some_workers##":"Esta serrería está trabajando por debajo de su capacidad máxima. Como resultado, la producción de madera será algo inferior."
"##lumber_mill_no_workers##":"Esta serrería no tiene empleados. La producción se ha detenido."
"##lumber_mill##":"Serrería"
"##macedonian_soldier##":"Soldado macedonio"
"##mainmenu_loadgame##":"Cargar partida"
"##mainmenu_loadmap##":"Nuevo mapa"
"##mainmenu_newgame##":"Nueva partida"
"##mainmenu_playmission##":""
"##mainmenu_quit##":"Salir"
"##marble##":"Mármol"
"##market_about##":"Nuestros mercados hacen llegar a todos los ciudadanos con dinero las maravillas del imperio. Todas las casas necesitan tener acceso a un mercado, aunque nadie quiere vivir demasiado cerca."
"##market_full_work##":"Este mercado está en funcionamiento."
"##market_kid_say_1##":"Esa gorda de ahí delante me ha dicho que lleve esto y que le siga."
"##market_no_workers##":"Este mercado no está en funcionamiento, y no suministra productos a la comunidad local."
"##market_search_food_source##":"Este mercado tiene mercaderes pero están buscando una fuente de alimentos que vender."
"##market##":"Mercado"
"##marketBuyer_average_life##":"No está mal la ciudad."
"##marketBuyer_gods_angry##":"¡Vivo en una ciudad pagana! ¡El gobernador no respeta a los dioses!"
"##marketBuyer_good_life##":"¡Buenos días, ciudadano! Menuda ciudad ¿eh?"
"##marketBuyer_high_workless##":"Con tanto desempleo, tengo que trabajar duro para conservar mi trabajo."
"##marketBuyer_low_entertainment##":"¡Esta debe ser la ciudad más aburrida de todo el Imperio!"
"##marketBuyer_need_workers##":"Nunca había visto tantos edificios tan necesitados de mano de obra."
"##marketBuyer_so_hungry##":"¡Apenas hay comida! ¿Cómo se supone que voy a ganarme la vida?"
"##marketKid_say_2##":"¡Esta cesta me está matando! ¡Me da igual para quién sea la comida, debería haber una ley contra el empleo infantil!"
"##marketKid_say_3##":"¡Saludos! Llevo esta cesta de comida al mercado de esa señora. ¡Espero llevarme una buena propina!"
"##marketLady_find_goods##":"Me marcho a por más existencias."
"##marketLady_return##":"¡Estas cestas pesan tanto! Llevo mercancía fresca devuelta a mí mercado."
"##mars_desc##":"Guerra"
"##max_available##":"Entretiene a"
"##maximizeBtnTooltip##":""
"##may_collect_about##":"genera aproximadamente"
"##meadow_caption##":"Pradera"
"##meat_farm_bad_work##":"Aquí trabajan muy pocos granjeros. Como resultado, la producción de carne es lenta."
"##meat_farm_bad_work##":"Con tan pocos granjeros, el ganado es pequeño y su crecimiento es extremadamente lento."
"##meat_farm_full_work##":"Esta granja tiene todo el personal que necesita. Su ganado es abundante y está bien alimentado."
"##meat_farm_info##":"Los ciudadanos bien alimentados quieren comer carne. Ésta puede almacenarse en graneros para el consumo local o transportarse a almacenes para ser exportada posteriormente."
"##meat_farm_need_some_workers##":"Esta granja tiene muy poco personal, Los cerdos tienen pocas crías que además crecen lentamente."
"##meat_farm_no_workers##":"Esta granja no tiene empleados. Los animales han escapado o han muerto."
"##meat_farm_patrly_workers##":"Esta granja está trabajando por debajo de su capacidad máxima. Como resultado, la producción de carne será algo inferior."
"##meat_farm##":"Granja de animales"
"##meat##":"Carne"
"##mercury_desc##":"Comercio"
"##mercury##":"Mercurio"
"##message##":"Mensaje"
"##messageBtnTooltip##":"Mensajes de tus escribas."
"##messageBtnTooltip##":""
"##messages##":"Mensajes"
"##migration_broke_tax##":"Los altos impuestos alejan a la gente de tu ciudad."
"##migration_broke_workless##":"El elevado desempleo en la ciudad está retardando la puntuación de prosperidad."
"##migration_empty_granary##":"La falta de alimentos frena la inmigración"
"##migration_lack_crime##":"El alto índice de criminalidad asusta a los habitantes."
"##migration_lack_empty_house##":"La falta de viviendas vacías está limitando la inmigración."
"##migration_lack_tax##":"Los elevados impuestos son un problema."
"##migration_lack_workless##":"El desempleo está reduciendo el número de inmigrantes."
"##migration_lessfood_granary##":"La falta de alimentos en los graneros está reduciendo la inmigración."
"##migration_middle_lack_tax##":"Los elevados impuestos frenan la inmigración"
"##migration_middle_lack_workless##":"El alto desempleo es un problema."
"##migration_middle_lack_workless##":"La falta de empleo impide la inmigración"
"##migration_people_away##":"La falta de trabajo está alejando a la gente."
"##military_academy_no_workers##":"Cuando los nuevos soldados finalizan el entrenamiento básico en los barracones, tratan de mejorar en capacidad y calidad en esta academia, pero no lo consiguen hasta que ésta ve satisfechas sus necesidades."
"##military_academy##":"Academia militar"
"##minimap_tooltip##":"Haz clic sobre este mapa para desplazarte a las zonas más lejanas de la ciudad"
"##minimizeBtnTooltip##":""
"##missing_barber_degrade##":"Esta casa decaerá pronto, puesto que ha perdido el acceso a la barbería."
"##missing_barber##":"Esta casa no puede mejorar, puesto que no tiene acceso local a una barbería."
"##missing_bath_degrade##":"Esta casa decaerá pronto, puesto que ha perdido el acceso a los baños."
"##missing_bath##":"Esta casa no puede mejorar, puesto que no tiene acceso a un baño local."
"##missing_college_degrade##":"Esta casa decaerá pronto. Su antiguo acceso a la educación se ha visto mermado al perder el acceso a una academia."
"##missing_college##":"Esta casa no puede mejorar, puesto que su excelente acceso a la educación debe mejorarse con el establecimiento de una academia."
"##missing_doctor__degrade##":"Esta casa decaerá pronto, puesto que se ha agotado su provisión sanitaria. Hay acceso local a un hospital, pero es difícil encontrar consultas médicas."
"##missing_doctor_or_hospital_degrade##":"Esta casa decaerá pronto, puesto que su oferta sanitaria es precaria. No sólo carece de consultas, sino que la cobertura hospitalaria deja también bastante que desear."
"##missing_doctor_or_hospital##":"Esta casa no puede evolucionar, ya que no tiene las mínimas condiciones sanitarias. No tiene acceso a una consulta ni a un hospital."
"##missing_doctor##":"Esta casa no puede evolucionar, ya que necesita mejor previsión sanitaria. Tiene acceso local a un hospital, pero debes construir una consulta médica cerca."
"##missing_entertainment_also##":"Esta casa no puede mejorar, puesto que hay muy poco entretenimiento en la zona."
"##missing_entertainment_amph##":"Esta casa no puede mejorar, puesto que apenas hay entretenimiento en la zona."
"##missing_entertainment_colloseum##":"Esta casa no puede mejorar, puesto que puede encontrarse algo de entretenimiento en la ciudad, pero no resulta suficiente."
"##missing_entertainment_degrade##":"Esta casa decaerá pronto, puesto que apenas hay entretenimiento en la zona."
"##missing_entertainment_need_more##":"Esta casa no puede mejorar, puesto que pese al buen entretenimiento que ofrece la ciudad, aún no hay una variada oferta."
"##missing_entertainment_patrician##":"Esta casa no puede mejorar, puesto que pese a la excelente oferta de entretenimientos, las instalaciones recreativas están demasiado concurridas o les falta variedad para atraer a los patricios."
"##missing_entertainment##":"Esta casa no puede mejorar, puesto que no hay entretenimiento en la zona."
"##missing_food_degrade##":"Esta casa decaerá pronto, puesto que últimamente no ha recibido suministro de alimentos procedente de un mercado local."
"##missing_food_degrade##":"Esta casa decaerá pronto. Aunque tiene acceso a un mercado, el mercado tiene problemas para obtener alimentos."
"##missing_food_from_market##":"Esta casa no puede mejorar. Aunque tiene acceso a un mercado local, el mercado mismo tiene problemas para obtener alimentos."
"##missing_food##":"Esta casa no puede mejorar, puesto que necesita suministro de alimentos de un mercado local."
"##missing_fountain_degrade##":"Esta casa decaerá pronto, puesto que no tiene acceso al agua limpia de una fuente."
"##missing_fountain##":"Esta casa no puede mejorar, puesto que no tiene acceso al agua limpia de una fuente."
"##missing_furniture_degrade##":"Esta casa decaerá pronto, puesto que se le han agotado los muebles y su mercado local está casi sin existencias."
"##missing_furniture##":"Esta casa no puede mejorar. Necesita muebles procedentes del mercado local antes de que un ciudadano más rico se mude a ella."
"##missing_hospital_degrade##":"Esta casa decaerá pronto, puesto que se ha agotado su provisión sanitaria. La cobertura de consultas médicas es buena, pero no hay acceso local a un hospital."
"##missing_hospital##":"Esta casa no puede evolucionar, ya que necesita mejor previsión sanitaria. La cobertura de la consulta médica es buena, pero no hay acceso a un hospital."
"##missing_library_degrade##":"Esta casa decaerá pronto. Su antiguo acceso a la educación se ha visto mermado y ha perdido el acceso a una biblioteca."
"##missing_library##":"Esta casa no puede mejorar, puesto que su acceso a la educación debe mejorarse con el acceso a una biblioteca."
"##missing_market_degrade##":"Esta casa decaerá pronto. Ha perdido su acceso al mercado."
"##missing_market##":"Esta casa no puede mejorar, puesto que no tiene acceso a un mercado local."
"##missing_oil_degrade##":"Esta casa decaerá pronto, puesto que se le ha agotado el aceite y su mercado local está sin existencias."
"##missing_oil##":"Esta casa no puede mejorar. Necesita aceite procedente del mercado local antes de que un ciudadano más rico se mude a ella."
"##missing_pottery_degrade##":"Esta casa decaerá pronto puesto que se le ha agotado la cerámica y su mercado local tiene pocas existencias."
"##missing_pottery##":"Esta casa no puede mejorar. Necesita cerámica procedente del mercado local para que un ciudadano más rico se mude a ella."
"##missing_religion_degrade##":"Esta casa decaerá pronto, puesto que ha perdido todo acceso a las instalaciones religiosas."
"##missing_religion##":"Esta casa no puede mejorar, puesto que no tiene acceso a instalaciones religiosas."
"##missing_school_degrade##":"Esta casa decaerá pronto. Su antiguo acceso a la educación se ha visto mermado y ha perdido el acceso a un colegio."
"##missing_school##":"Esta casa no puede mejorar, puesto que no cuenta con una oferta educativa ni con un colegio ni con una biblioteca."
"##missing_school##":"Esta casa no puede mejorar, puesto que su acceso a la educación debe mejorarse con el acceso a un colegio."
"##missing_second_food_degrade##":"Esta casa decaerá pronto, puesto que actualmente sólo tiene acceso a un tipo de alimento procedente del mercado local. Este hecho desalienta a las clase plebeya más acomodada."
"##missing_second_food##":"Esta casa no puede mejorar, puesto que necesita una segunda fuente de alimentos, procedente de un mercado local, para animar a las clases más acomodadas a mudarse a ella."
"##missing_second_religion_degrade##":"Esta casa decaerá pronto. Su acceso a las instalaciones religiosas se ha visto reducido a los templos de un solo dios."
"##missing_second_religion##":"Esta casa no puede mejorar, puesto que solamente tiene acceso a templos de un solo dios. Cuando sus habitantes prosperen, querrán emplear su tiempo en adorar a más dioses."
"##missing_second_wine##":"Esta casa no puede mejorar. Necesita un segundo tipo de vino para satisfacer el decadente estilo de vida de los desocupados patricios. Abre una nueva ruta comercial o fabrica tu propio vino."
"##missing_third_food_degrade##":"Esta casa decaerá pronto, puesto que actualmente sólo tiene acceso a dos tipos de alimentos procedentes del mercado local. Este hecho desalienta a la clase patricia"
"##missing_third_food##":"Esta casa no puede mejorar, puesto que necesita una tercera fuente de alimentos procedente de un mercado local para animar a los patricios a mudarse a ella."
"##missing_third_religion_degrade##":"Esta casa decaerá pronto. Sus instalaciones religiosas, antes excelentes, se han visto reducidas a los templos de sólo dos dioses."
"##missing_third_religion##":"Esta casa no puede mejorar, puesto que sólo tiene acceso a templos de dos dioses. Cuando sus habitantes prosperen, querrán emplear su tiempo en adorar a más dioses."
"##missing_water_degrade##":"Esta casa decaerá pronto, puesto que no tiene acceso ni a las formas más primitivas de abastecimiento de agua."
"##missing_water##":"Esta casa no puede mejorar, puesto que no tiene acceso ni a las formas más primitivas de abastecimiento de agua."
"##missing_wine_degrade##":"Esta casa decaerá pronto, puesto que se le ha agotado el vino y su mercado local está casi sin existencias."
"##missing_wine##":"Esta casa no puede mejorar. Necesita vino procedente del mercado local antes de que un ciudadano más rico se mude a ella."
"##mission_win##":"Victoria"
"##mission_wnd_population##":"Población"
"##mission_wnd_targets_title##":"Objetivos"
"##mission_wnd_tocity##":"A la ciudad"
"##missionBtnTooltip##":"Título de mapa de provincia."
"##month_1_short##":"Ene"
"##month_10_short##":"Oct"
"##month_11_short##":"Nov"
"##month_12_short##":""
"##month_2_short##":""
"##month_3_short##":"Mar"
"##month_4_short##":""
"##month_5_short##":"May"
"##month_6_short##":"Jun"
"##month_7_short##":"Jul"
"##month_8_short##":""
"##month_9_short##":"Sep"
"##month##":"Persona"
"##months_until_defeat##":"meses para la derrota"
"##months_until_victory##":"meses para la victoria"
"##months##":"Gente"
"##more_people##":"Empleados"
"##more_person##":"Empleado"
"##my_rome##":""
"##nearby_building_negative_effect_degrade##":"Un edificio cercano está influyendo negativamente sobre el atractivo de este lugar. Intenta colocar jardines, estatuas o plazas, por ejemplo."
"##nearby_building_negative_effect##":"Un edificio cercano está influyendo negativamente sobre el atractivo de este lugar. Intenta colocar jardines, estatuas o plazas, por ejemplo."
"##need_access_to_full_reservoir##":"Necesita acceso a un depósito lleno para funcionar."
"##need_actor_colony##":"Construye una colonia de actores para los intérpretes."
"##need_barracks_for_work##":"Necesita barracones activos para recibir soldados."
"##need_build_on_cleared_area##":"Debe construirse en terreno despejado"
"##need_build_on_cleared_area##":""
"##need_charioter_school##":"Construye una escuela de aurigas para celebrar carreras."
"##need_clay_pit##":"Este edificio necesita arcilla."
"##need_grape##":"Este edificio necesita uvas."
"##need_iron_for_work##":"Este edificio necesita mineral de hierro."
"##need_iron_mine##":"Construye una mina de hierro."
"##need_lionnursery##":"Construye una casa de fieras para peleas de animales."
"##need_marble_for_large_temple##":"Necesitas dos carros de mármol para construir un gran templo."
"##need_olive_farm##":"Construye una granja de olivas."
"##need_population##":"necesarios)"
"##need_reservoir_for_work##":"Esta fuente no funcionará a menos que consiga tener acceso a un depósito cercano."
"##need_temples_for_city##":"Los ciudadanos muestran interés por la religión. La falta de acceso a un lugar de culto cercano está mermando el desarrollo de la ciudad."
"##need_timber_mill##":"Construye una serrería."
"##need_trainee_charioteer##":"El hipódromo no cuenta con cuadrigas. Tener algunas supondría un incremento de la población de la ciudad, que está ávida de más entretenimiento."
"##need_vines_farm##":"Construye una granja de vino."
"##neptune_desc##":"El mar"
"##new_festival##":"Celebrar nuevo festival."
"##new_governor##":""
"##new_map##":"Archivo"
"##new_trade_route_to##":"Establecida nueva ruta comercial."
"##no_academy_access##":"Esta casa no tiene acceso a una academia."
"##no_culture_building_in_city##":"No tienes cultura en la ciudad, ergo (palabra latina para "por lo tanto") no tienes puntuación de cultura."
"##no_dock_for_sea_trade_routes##":""
"##no_goods_for_request##":"No tienes bienes suficientes en los almacenes."
"##no_people_in_city##":"¡No hay gente en la ciudad!"
"##no_space_for_evolve##":"Si tuviese más espacio hacia donde expandirse, esta vivienda pronto se convertiría en una de mayor categoría."
"##no_target_population##":"( Sin población objetivo )"
"##no_tax_in_this_year##":"No se han recaudado impuestos en esta casa este año."
"##no_visited_by_taxman##":"Sin visitar por recaudador. No paga impuestos"
"##northBtnTooltip##":""
"##not_available##":"¡Aún no disponible!"
"##numidian_warrior##":"Guerrero numidio"
"##occupants##":"ocupantes"
"##oil_workshop_bad_work##":"Aquí trabaja muy poco personal. Como resultado, la producción de aceite es lenta."
"##oil_workshop_bad_work##":"Con tan poco personal en esta prensa, la producción es casi inexistente. Producirá muy poco aceite el próximo año."
"##oil_workshop_full_work##":"Esta prensa tiene todo el personal que necesita y produce mucho aceite de alta calidad."
"##oil_workshop_info##":"Aquí se prensan las olivas para obtener aceite, aceite que necesitan los plebeyos para cocinar e iluminar sus estancias. Comerciar con el excedente permitirá obtener buenos beneficios."
"##oil_workshop_need_resource##":"Esta prensa no producirá aceite si no recibe olivas de un almacén o de una granja."
"##oil_workshop_need_some_workers##":"Esta prensa podría usar más trabajadores para alcanzar su máxima tasa de producción de aceite."
"##oil_workshop_no_workers##":"Esta prensa de olivas no tiene empleados. No producirá aceite."
"##oil_workshop_patrly_workers##":"Esta prensa tiene muy poco personal, por lo que la producción de aceite es más lenta de lo deseado."
"##oil_workshop##":"Taller de aceite"
"##oil##":"Aceite"
"##ok##":"Aceptar"
"##olive_factory_stock##":"Olivas almacenadas,"
"##olive_farm_bad_work##":"Con tan pocos granjeros, la mayoría de los olivos no tiene fruto."
"##olive_farm_full_work##":"Esta granja tiene todo el personal que necesita. Normalmente las ramas de los árboles se doblan por el peso de las aceitunas."
"##olive_farm_info##":"Las olivas son altamente valiosas por el aceite. Las prensas utilizan las olivas para elaborar aceite para la cocina, iluminación, lubricación y conservación."
"##olive_farm_need_some_workers##":"Esta granja está trabajando por debajo de su capacidad máxima. Con más trabajadores la producción de olivas podría ser mejor."
"##olive_farm_patrly_workers##":"Esta granja tiene muy poco personal, por lo que la producción de olivas es más lenta de lo deseado."
"##olive_farm_slow_work##":"Aquí trabajan muy pocos granjeros. Como resultado, la producción de olivas es lenta."
"##olive_farm##":"Granja de olivas."
"##olive##":"Olivas"
"##open_trade_route##":"Abrir ruta comercial"
"##options##":"Opciones"
"##oracle_info##":"Este templo de mármol hace que las casas adyacentes sean más atractivas, además de agradar a todos los dioses."
"##oracle_need_2_cart_marble##":"Necesitas dos carros de mármol para construir un oráculo."
"##oracle##":"Oráculo"
"##oracles_in_city##":"Oráculos en la ciudad"
"##oracles##":"Oráculos"
"##other##":"Varios"
"##our_foods_level_are_low##":"Nuestros niveles de alimentos son bajos"
"##out_of_credit##":"¡Sin fondos!"
"##overlays##":"Capas"
"##ovrm_aborigen##":""
"##ovrm_academy##":""
"##ovrm_amphitheater##":"Anfiteatros"
"##ovrm_barber##":""
"##ovrm_baths##":""
"##ovrm_clinic##":""
"##ovrm_colloseum##":""
"##ovrm_commerce##":""
"##ovrm_crime##":""
"##ovrm_damage##":""
"##ovrm_desirability##":""
"##ovrm_education##":"Todo"
"##ovrm_educations##":"Educación"
"##ovrm_entertainments##":""
"##ovrm_entrertainment##":"Todo"
"##ovrm_fire##":""
"##ovrm_food##":""
"##ovrm_health##":""
"##ovrm_hippodrome##":""
"##ovrm_hospital##":""
"##ovrm_library##":"Biblioteca"
"##ovrm_religion##":""
"##ovrm_risks##":""
"##ovrm_school##":""
"##ovrm_simple##":""
"##ovrm_tax##":""
"##ovrm_text##":"Capas"
"##ovrm_theater##":""
"##ovrm_tooltip##":""
"##ovrm_troubles##":""
"##ovrm_water##":""
"##partician_good_life##":"Esta ciudad está bien gobernada."
"##partician_need_workers##":"¡Se nota en el sector de servicios! ¡La ciudad necesita más trabajadores!"
"##patients#":"pacientes"
"##patrician_average_life##":"Desde el confort de mi villa observo que la vida de la ciudad es muy buena."
"##patrician_gods_angry##":"¡Ciudadano! Las cosas no marchan. Los dioses están enfadados."
"##patrician_high_workless##":"¡Qué desgracia! ¡Nunca había visto a tantos plebeyos desempleados!"
"##patrician_low_entertainment##":"¡Ciudadano! ¿No es esta la ciudad más aburrida del Imperio?"
"##patrician_so_hungry##":"¡Salve! ¿De qué me sirve ser rico, si no hay comida que comprar?"
"##pay_to_open_trade_route?##":"¿Pagar para abrir esta ruta?"
"##people##":"Denarios"
"##percents##":"Interés en"
"##person##":"Denario"
"##plaza_caption##":"Plaza"
"##plaza##":"Plaza"
"##plname_continue##":"Continuar"
"##pn_salary##":"Salario personal"
"##pop##":"Pobl"
"##population_registered_as_taxpayers##":"de la población son contribuyentes"
"##population_tooltip##":"Población actual de la ciudad"
"##population##":"Población"
"##pottery_bad_work##":"Aquí trabaja muy poca gente. Como resultado, la producción de cerámica es lenta."
"##pottery_workshop_bad_work##":"Con tan poco personal en este taller, la producción es casi inexistente. Producirá muy poca cerámica el próximo año."
"##pottery_workshop_full_work##":"Este taller tiene todo el personal que necesita. Se esmera en producir cerámica."
"##pottery_workshop_info##":"Aquí, los ceramistas dan forma a las vasijas usadas por los ciudadanos para almacenar productos. Comercia con la cerámica o permite su distribución en tus mercados para una mejora de las viviendas."
"##pottery_workshop_need_resource##":"Este taller necesita recibir arcilla de un almacén o de un pozo de arcilla para producir cerámica."
"##pottery_workshop_need_som_workers##":"Este taller tiene muy poco personal, por lo que la producción de cerámica es más lenta de lo deseado."
"##pottery_workshop_patrly_workers##":"Este taller está trabajando por debajo de su capacidad máxima. Como resultado, la producción de cerámica será algo inferior."
"##pottery_workshop##":"Taller de cerámica"
"##pottery##":"Cerámica"
"##praetor_salary##":"Salario de pretor de"
"##prefect_fight_fire##":"Es impresionante el calor que da este incendio."
"##prefect_gods_angry##":"¡Temo que los dioses maldigan la ciudad!"
"##prefect_good_life##":"Es un lugar fantástico para vivir."
"##prefect_high_workless##":"¡Nunca había visto a tanta gente sin trabajo!"
"##prefect_high_workless##":""
"##prefect_low_entertainment##":"¡Los criminales que atrapo se divierten más que los ciudadanos!"
"##prefect_need_workers##":"¡La ciudad necesita urgentemente más trabajadores!"
"##prefect_so_hungry##":"No hay suficiente comida en la ciudad y eso propicia los delitos."
"##prefecture_bad_work##":"Sólo contamos con personal de oficina. A veces pasa un mes sin que mandemos un prefecto a la calle."
"##prefecture_full_work##":"Nuestra agenda de servicios está llena. Nuestros prefectos siempre están recorriendo las calles."
"##prefecture_info##":"Las prefecturas envían prefectos a la ciudad para mantener la paz y para luchar contra el fuego. El orden cívico sólo es posible cuando los prefectos patrullan la ciudad."
"##prefecture_need_some_workers##":"Estamos algo escasos de prefectos. Tenemos huecos de varios días en nuestra agenda."
"##prefecture_no_workers##":""
"##prefecture_on_patrol##":"Nuestro prefecto está patrullando."
"##prefecture_patrly_workers##":"Tenemos poco personal, y tenemos huecos peligrosos de hasta una semana en nuestra agenda de servicios."
"##prefecture_ready_for_work##":"Nuestro prefecto se prepara para el trabajo."
"##prefecture_slow_work##":"Tenemos muy poco personal. A veces no hay prefecto en dos semanas."
"##prefecture##":"Prefectura"
"##press_escape_to_exit##":"Haz clic derecho para salir"
"##priest_gods_angry##":"¡Corremos peligro! ¡En esta ciudad no se respeta a los dioses!"
"##priest_good_life##":"La vida es buena en la ciudad."
"##priest_good_life##":"Esta ciudad es un buen sitio."
"##priest_high_workless##":"Hay una tasa de desempleo preocupantemente alta."
"##priest_low_entertainment##":"La ciudad es aburrida. ¡Incluso a los sacerdotes nos gusta ver pelear a los gladiadores de vez en cuando!"
"##priest_need_workers##":"La ciudad necesita más trabajadores."
"##priest_so_hungry##":"¡Saludos! La ciudad necesita comida urgentemente."
"##priority_button_tolltip##":"Haz clic sobre un número para establecer un nivel de prioridad. Las demás prioridades se ajustarán de acuerdo a éste"
"##proconsoul_salary##":"Salario de procónsul de"
"##procurator_salary##":"Salario de procurador de"
"##profit##":"Flujo neto int./ext."
"##qty_stacked_in_city_warehouse##":"en almacenes"
"##quaestor_salary##":"Salario de cuestor de"
"##quarry_bad_work##":"Con tan pocos empleados, la producción es inexistente. Se extraerá muy poco mármol el próximo año."
"##quarry_full_work##":"Esta cantera tiene todo el personal que necesita y se esmera en extraer mármol."
"##quarry_info##":"Extrae el mármol de las rocas próximas y empléalo para construir oráculos y templos grandes. Generalmente el mármol es un valioso bien de exportación."
"##quarry_need_some_workers##":"Esta cantera está trabajando por debajo de su capacidad debido a una ligera escasez de trabajadores."
"##quarry_no_workers##":"Esta cantera no tiene empleados. La producción se ha detenido."
"##quarry_patrly_workers##":"Aquí trabaja muy poco personal. Como resultado, la extracción de mármol es muy lenta."
"##quarry_slow_work##":"Aquí trabaja muy poco personal. Como resultado, la extracción de arcilla es extremadamente lenta."
"##quarry##":"Cantera de mármol"
"##quit##":"Salir"
"##rating##":"Puntuaciones"
"##rawm_production_complete_m##":"La producción está en un"
"##really_destroy_fort##":"¿Seguro que deseas poner este fuerte fuera de servicio?"
"##recruter_gods_angry7##":"¡Tenemos problemas! ¡Los dioses están irritados con nosotros!"
"##recruter_good_life##":"¡Salve! La vida aquí es genial."
"##recruter_high_workless##":"¿Has visto cuánto desempleo hay?"
"##recruter_low_entertainment##":"Trabajo duro y luego me gusta relajarme. Aquí no puedo ¡No hay diversiones!"
"##recruter_need_workers##":"¡Ciudadano! ¡La ciudad necesita más trabajadores!"
"##recruter_normal_life##":"¡Saludos! La ciudad está bien, en serio."
"##recruter_so_hungry##":"¿Puedes darme un poco de pan? Llevo mucho tiempo sin comer."
"##reject##":"Rechazar bienes"
"##religion_access_1_temple##":"Esta casa tiene acceso a un templo de un solo dios."
"##religion_access_2_temple##":"Esta casa tiene acceso a templos de 2 dioses diferentes."
"##religion_access_3_temple##":"Esta casa tiene acceso a templos de 3 dioses diferentes."
"##religion_access_4_temple##":"Esta casa tiene acceso a templos de 4 dioses diferentes."
"##religion_access_5_temple##":"Esta casa tiene acceso a templos de todos los dioses."
"##religion_access_full##":"Esta casa tiene acceso a un oráculo y a templos de todos los dioses."
"##religion_advisor##":"Estructuras religiosas"
"##religion_no_access##":"Esta casa no tiene acceso ni a templos ni a oráculos."
"##religion##":"Religión"
"##replay_game##":"Volver a jugar el mapa"
"##request_btn_tooltip##":"Haz clic aquí para hacer una solicitud."
"##requierd##":"Necesidad"
"##reservoir_info##":"Esta cisterna gigante almacena agua potable que se distribuye a través de tuberías de arcilla a todo el radio urbano. Los acueductos pueden conectar depósitos que se encuentran a gran distancia."
"##reservoir_no_water##":"Para ser operativo, este depósito tiene que estar cerca del agua o conectado mediante un acueducto a un depósito en funcionamiento."
"##reservoir##":""
"##rift_info##":"Grietas en la tierra"
"##right_click_to_exit##":"Haz clic derecho para salir"
"##rioter_say_1##":"¡Ya que el gobernador no se preocupa por mi, voy a enseñarle lo que yo me preocupo por su ciudad!"
"##rioter_say_3##":"¿Sabes cómo arden las ciudades? Observa atentamente."
"##rladv_mood##":"El dios está"
"##road_caption##":"Carretera"
"##road_text##":"Las carreteras son fundamentales para el buen funcionamiento de la ciudad, cuyos habitantes solamente saldrán de sus casas para internarse en ellas."
"##roadBtnTooltip##":""
"##rock_caption##":"Rocas"
"##rock_text##":"Las rocas son infranqueables y no pueden eliminarse. Las canteras de mármol y las minas de hierro sólo pueden funcionar si se encuentran al lado de las rocas."
"##roman_city##":"Ciudad romana"
"##rome_prices##":"Precios establecidos por Roma"
"##romeGuard_average_life##":"Todo parece tranquilo."
"##romeGuard_gods_angry##":"¡Los dioses se han enfadado con la ciudad!"
"##romeGuard_good_live##":"¡No soy más que un humilde soldado, pero sé que esta es una gran ciudad!"
"##romeGuard_high_workless##":"Con tanto paro doy gracias a los dioses por mi empleo."
"##romeGuard_low_entertainment##":"Si hubiera más diversiones en la ciudad, mi trabajo no resultaría tan aburrido."
"##romeGuard_need_workers##":"¡La ciudad necesita muchos más trabajadores!"
"##romeGuard_so_hungry##":"¿Cómo va a luchar un soldado con el estómago vacío?"
"##rotateLeftBtnTooltip##":""
"##rotateRightBtnTooltip##":""
"##samnite_soldier##":"Soldado samnita"
"##save_city##":"Guardar partida"
"##save_game_here##":"Guardar la partida actual en este archivo"
"##save_map##":"Cargar mapa"
"##scholar_average_life##":"La ciudad no está mal."
"##scholar_gods_angry##":"¡Socorro! ¡Los dioses están tan enfadados que nos van a destruir!"
"##scholar_good_life##":"¡La ciudad es genial!"
"##scholar_high_workless##":"¡Aquí siempre hay tanta gente buscando trabajo!"
"##scholar_low_entertainment##":"¡La ciudad es aburrida! ¡Quiero más espectáculos!"
"##scholar_need_workers##":"¡Hay tan pocos trabajadores que hasta a mi me han ofrecido un puesto!"
"##scholar_so_hungry##":"¡Me muero de hambre!"
"##scholar##":"Alumno"
"##scholars##":"en edad escolar,"
"##school_full_access##":"Esta casa tiene acceso a un colegio."
"##school_full_work##":"Este colegio no está en funcionamiento, y no sirve a la comunidad local."
"##school_full_work##":"Este colegio está en funcionamiento. Los niños son cultos y están instruidos."
"##school_info##":"Los niños han de ir a los colegios cercanos para aprender las bases de la lectura, la escritura y la retórica si desean convertirse en adultos de provecho."
"##school_no_access##":"Esta casa no tiene acceso a un colegio."
"##school##":"Colegio"
"##schools##":"Colegios"
"##screen_settings##":"Configuración imagen"
"##scribes_messages_title## ":"Ver mensajes"
"##scrive_messages_title##":"Mensajes de tus escribas."
"##sdlr_bold##":"Intrépidos"
"##seamerchant_noany_trade##":"Si por mi fuera, nunca vendríamos a esta ciudad. ¡Nunca compran nada, ni tienen nada que vender!"
"##select_city_layer##":"Seleccionar un informe superpuesto de la ciudad"
"##select_location##":""
"##seleucid_soldier##":"Soldado seleucida"
"##sell_price##":"Los vendedores reciben"
"##senate_1_info##":"El Senado se encuentra entre los edificios más atractivos de la ciudad. Da trabajo a recaudadores de impuestos y guarda sus ingresos, a la vez que realiza un seguimiento de las estadísticas primordiales."
"##senate_1##":"Senado"
"##senate_save##":"en los tesoros de la ciudad"
"##senateBtnTooltip##":""
"##senatepp_clt_rating##":"Cultura"
"##senatepp_favour_rating##":"Favor"
"##senatepp_peace_rating##":"Paz"
"##senatepp_prsp_rating##":"Prosperidad"
"##senatepp_unemployment##":"Desempleo"
"##send_generous_gift##":"Mandar regalo generoso"
"##send_lavish_gift##":"Mandar regalo lujoso"
"##send_legion_to_emperor##":"Ordena a tu asesor militar que asigne algunas legiones operativas al servicio del Imperio."
"##send_modest_gift##":"Mandar regalo modesto"
"##send_money##":"Donar este dinero a la ciudad desde tus ahorros personales"
"##send_to_city##":"Dar a la cuidad"
"##set_mayor_salary##":"Haz clic aquí para establecer tu salario personal"
"##shipyard_info##":"Con algunos carros llenos de madera y los suficientes trabajadores, el astillero construye barcos de pesca para las dársenas de la ciudad."
"##shipyard_notneed_ours_boat##":"Actualmente no hay dársenas pesqueras que nos soliciten barcos."
"##shipyard##":"Astillero"
"##show_bigpanel##":"Mostrar el panel de control completo"
"##show_prices##":"Mostrar precios"
"##show##":"Espectáculos"
"##simple_formation_text##":"Formación simple, que ofrece cierta ventaja a las tropas en actitud defensiva."
"##sldh_health_sparse##":"Escasa"
"##sldh_health_strong##":"Fuertes"
"##sldh_health_strongest##":"Muy fuertes"
"##sldr_badly_shaken##":"Muy desconcertados"
"##sldr_daring##":"Audaces"
"##sldr_encouraged##":"Animados"
"##sldr_extremely_scared##":"Sumamente asustados"
"##sldr_shaken##":"Desconcertados"
"##sldr_terrified##":"Aterrorizados"
"##sldr_totally_distraught##":"Totalmente afligidos"
"##sldr_very_bold##":"Muy intrépidos"
"##sldr_very_frightened##":"Muy asustados"
"##small_ceres_temple##":"Templo de Ceres"
"##small_domus##":"Apartamentos pequeños"
"##small_food_on_next_month##":"muy poca comida para el próximo mes."
"##small_hovel##":"Tugurio pequeño"
"##small_hut##":"Casa pequeña"
"##small_mars_temple##":"Templo de Marte"
"##small_mercury_temple_info##":"Los mercaderes invocan a Mercurio para proteger sus tiendas y almacenes. Cuando Mercurio está enfadado, los ingresos de todo el mundo corren peligro."
"##small_mercury_temple##":"Templo de Mercurio"
"##small_neptune_temple##":"Templo de Neptuno"
"##small_palace##":"Palacio pequeño"
"##small_shack##":"Chabola pequeña"
"##small_temples##":"Templos pequeños"
"##small_tent##":"Tienda pequeña"
"##small_venus_temple##":"Templo de Venus"
"##small_villa##":"Villa pequeña"
"##small##":"pequeño"
"##smcurse_of_venus_description##":"Venus, portavoz del amor y la armonía, está triste. Esto no es bueno para los prefectos de la ciudad."
"##soldier##":"Soldado"
"##soldiers_health##":"Salud de los soldados"
"##soldiers_in_legion##":"Soldados en legión"
"##soldiers##":"Soldados"
"##some_fire_risk##":"Riesgo de incendio mediano"
"##some_food_on_next_month##":"algo de comida para el próximo mes."
"##sound_settings##":"Configuración sonido"
"##spear##":"Lanza"
"##special_orders##":""
"##speed_settings##":"Configuración velocidad"
"##spirit_of_mars_text##":""
"##spirit_of_mars_title##":""
"##stacking_resource##":"Reservar recurso"
"##start_condition##":"Condiciones de comienzo"
"##statue_desc##":""
"##students##":"en edad universitaria."
"##tamer_normal_life##":"Si tocas a mi león, sentirás mi látigo en tus carnes."
"##target_population_is##":"( La población objetivo es"
"##tax_rate##":"Impuestos:"
"##taxCollector_average_life##":"Todo parece ir sobre ruedas."
"##taxCollector_gods_angry##":"¡Si no construimos templos pronto, los dioses maldecirán la ciudad!"
"##taxCollector_good_life##":"¡Saludos! Es muy agradable vivir en esta ciudad."
"##taxCollector_high_tax##":"¿Has visto que impuestos? Ciudadano, esto no es justo."
"##taxCollector_high_workless##":"Este sitio no me gusta. Hay demasiado paro."
"##taxCollector_low_entertainment##":"No me importaría estrujar hasta el último denario de esta gente si hubiera un poco más de diversión por las noches."
"##taxCollector_need_workers##":"La ciudad necesita más trabajadores."
"##taxCollector_so_hungry##":"¡Si no conseguimos comida pronto, no va a quedar nadie para pagar impuestos!"
"##taxCollector_very_little_tax##":"Estas casas pagan tan poco que casi no merece la pena visitarlas."
"##taxes##":"Impuestos"
"##taxСollector_low_tax_collected##":"Cuando recaudo impuestos a estos trabajadores me dan ganas de llorar. ¡Bueno, no tanto!"
"##taxСollector_much_tax##":"Me encanta recaudar impuestos en hogares adinerados como estos."
"##teacher_average_life##":"Esta ciudad se merece un 8 sobre 10."
"##teacher_gods_angry##":"¡Si no construimos más templos, sufriremos la ira de los dioses!"
"##teacher_good_life##":"¡Matrícula de honor! ¡La ciudad es maravillosa!"
"##teacher_high_workless##":"Espero no perder mi empleo. ¡Hay tanto paro que no sé si podría encontrar otro!"
"##teacher_low_entertainment##":"¡La ciudad es aburrida! Necesitamos más espectáculos."
"##teacher_need_workers##":"Es impresionante la cantidad de ofertas de trabajo existentes."
"##teacher_so_hungry##":"No hay suficiente comida. Los ciudadanos son infelices."
"##templeBtnTooltip##":""
"##temples##":"Templos"
"##theater_full_access##":"Esta casa tiene acceso a un teatro."
"##theater_full_work##":"Este teatro representa obras con actores locales, que por lo general atraen a una gran audiencia."
"##theater_need_actors##":"Los teatros y anfiteatros siempre están buscando nuevos talentos."
"##theater_no_access##":"Esta casa no tiene acceso a un teatro."
"##theater_no_have_any_shows##":"Este teatro nunca tiene representaciones. Necesita actores de verdad para entretener a la comunidad."
"##theater_no_workers##":"En este teatro no hay ni apuntador. Sin trabajadores, no hay representaciones de ningún tipo."
"##theater##":"Teatro"
"##theaters##":"Teatros"
"##this_year##":"Hasta ahora, en este año"
"##timber_factory_stock##":"Madera almacenada,"
"##timber_mill_need_trees##":"Construye serrerías cerca de los árboles."
"##timber##":"Madera"
"##to_trade_advisor##":"Al asesor comercial"
"##tooltip_full##":"Ayuda del ratón - Completa"
"##tooltip_some##":"Ayuda del ratón - Parcial"
"##tower_may_build_on_thick_walls##":"Solamente puedes construir torres en murallas gruesas."
"##tower##":"Torre"
"##trade_advisor_blocked_clay_production##":"Tu asesor comercial ha detenido la extracción de arcilla."
"##trade_advisor_blocked_fruit_production##":"Tu asesor comercial ha ordenado suspender el cultivo de frutas."
"##trade_advisor_blocked_furniture_production##":"Tu asesor comercial ha ordenado suspender la producción de muebles."
"##trade_advisor_blocked_grape_production##":"Tu asesor comercial ha ordenado suspender el cultivo de viñas."
"##trade_advisor_blocked_iron_production##":"Tu asesor comercial ha ordenado suspender la minería del hierro."
"##trade_advisor_blocked_marble_production##":"Tu asesor comercial ha ordenado suspender la producción de mármol."
"##trade_advisor_blocked_meat_production##":"Tu asesor comercial ha ordenado suspender la producción de carne de cerdo."
"##trade_advisor_blocked_oil_production##":"Tu asesor comercial ha ordenado que cese la producción de aceite."
"##trade_advisor_blocked_olive_production##":"Tu asesor comercial ha ordenado suspender el cultivo de olivos."
"##trade_advisor_blocked_pottery_production##":"Tu asesor comercial ha ordenado suspender la producción de cerámica."
"##trade_advisor_blocked_timber_production##":"Tu asesor comercial ha ordenado suspender la tala de árboles."
"##trade_advisor_blocked_vegetable_production##":"Tu asesor comercial ha ordenado suspender la producción de vegetales."
"##trade_advisor_blocked_weapon_production##":"Tu asesor comercial ha ordenado suspender la producción de armas."
"##trade_advisor_blocked_wheat_production##":"Tu asesor comercial ha ordenado suspender el cultivo de trigo."
"##trade_advisor_blocked_wine_production##":"Tu asesor comercial ha ordenado suspender la producción de vino."
"##trade_advisor##":"Asesor comercial."
"##trade_btn_notrade_text##":"Sin comerciar"
"##trade_caravan_from##":"Caravana de mercaderes de"
"##trade##":"Comercio"
"##trees_and_forest_caption##":"Árboles y bosques"
"##trees_and_forest_text##":"Los árboles son infranqueables. Deben talarse. Son vitales para la industria maderera y la serrería ha de estar junto a ellos."
"##trouble_colloseum_full_work##":"Este coliseo tiene combates de gladiadores y luchas con leones, para el deleite del público local."
"##trouble_colloseum_have_only_gladiatros##":"Este coliseo celebra luchas de gladiadores. Los leones añadirían variedad al combate a muerte."
"##trouble_colloseum_have_only_lions##":"Este coliseo celebra luchas con animales, utilizando leones criados aquí. También debería contar con gladiadores para los combates cuerpo a cuerpo."
"##trouble_colloseum_no_shows##":"Este coliseo no tiene espectáculos. Necesita gladiadores y leones para atraer a las multitudes."
"##trouble_farm_was_blighted_by_locust##":"Estas tierras han sufrido una plaga reciente de langostas, por lo que tardarán tiempo en recuperarse."
"##trouble_have_damage##":"Este edificio tiene algo de riesgo de derrumbarse."
"##trouble_hippodrome_full_work##":"Este hipódromo celebra frecuentes y emocionantes carreras para el deleite del público local."
"##trouble_hippodrome_no_charioters##":"Este hipódromo no tiene carreras. Se necesitan aurigas para entretener a las masas."
"##trouble_low_fire_risk##":"Este edificio no tiene posibilidades de incendiarse."
"##trouble_most_damage##":"Este edificio presenta numerosos fallos estructurales y grietas."
"##trouble_most_fire##":"Este edificio es una trampa incendiaria."
"##trouble_need_olive##":"Este edificio necesita olivas."
"##trouble_need_road_access##":"Este edificio necesita un acceso por carretera"
"##trouble_need_timber##":"Este edificio necesita madera."
"##trouble_no_damage##":"Este edificio está en perfectas condiciones estructurales."
"##trouble_some_damage##":"Este edificio tiene pocas posibilidades de derrumbarse."
"##trouble_some_fire##":"Este edificio tiene riesgo de incendio."
"##trouble_too_far_from_water##":"¡Este edificio no está cerca del agua!"
"##tutorial_prefecture_info##":""
"##unable_fullfill_request##":"Imposible satisfacer solicitud."
"##unit##":"año"
"##units_in_stock##":"Almacenando"
"##units##":"años"
"##use_and_trade_resource##":"Comerciar y utilizar este recurso"
"##vegetable_farm_full_work##":"Esta granja tiene todo el personal que necesita. Aquí, los cultivos crecen en abundancia."
"##vegetable_farm_info##":"Los vegetales son importantes en la dieta equilibrada que tu gente necesita para estar sana y feliz. Los graneros almacenan verdura, y los almacenes conservan el excedente para la exportación."
"##vegetable_farm_need_some_workers##":"Esta granja está falta de personal, por lo tanto sus vegetales tardarán más en crecer."
"##vegetable_farm_no_workers##":"Esta granja no tiene empleados. No se ha plantado nada."
"##vegetable_farm_patrly_workers##":"Esta granja está funcionando por debajo de su capacidad máxima. Como resultado, la producción de verdura será algo inferior."
"##vegetable_farm##":"Granja de vegetales"
"##vegetable##":"Vegetales"
"##venus_desc##":"Amor"
"##venus##":"Venus"
"##very_high_fire_risk##":"Riesgo de incendio muy alto"
"##very_low_fire_risk##":"Riesgo de incendio muy bajo"
"##vinard_bad_work##":"Con tan pocos granjeros, muy pocas uvas sobrevivirán."
"##vinard_full_work##":"Esta granja tiene todo el personal que necesita. Sus viñas dan uvas gordas y jugosas."
"##vinard_info##":"Las uvas de estos viñedos están especialmente indicadas para la elaboración de vino. Las bodegas podrán elaborar vino para los patricios, y también podrás exportar."
"##vinard_need_some_workers##":"Esta granja tiene muy poco personal, por lo que la producción de uvas es más lenta de lo deseado."
"##vinard_no_workers##":"Esta granja no tiene empleados. La producción se ha detenido."
"##vinard_patrly_workers##":"Esta granja está trabajando por debajo de su capacidad máxima. Como resultado, la producción de uvas será algo inferior."
"##vinard_slow_work##":"Aquí trabajan muy pocos granjeros. Como resultado, la producción de uvas es lenta."
"##vinard##":"Granja de uvas"
"##vinard##":"Granja de vino"
"##visit_chief_advisor##":"Visita al asesor jefe"
"##visit_education_advisor##":"Visita al asesor de educación"
"##visit_entertainment_advisor##":"Visita al asesor de entretenimiento"
"##visit_financial_advisor##":"Visita al asesor de finanzas"
"##visit_health_advisor##":"Visita al asesor de sanidad"
"##visit_imperial_advisor##":"Visita al asesor imperial"
"##visit_labor_advisor##":"Visita al asesor laboral"
"##visit_military_advisor##":"Visita al asesor militar"
"##visit_population_advisor##":"Visita al asesor demográfico"
"##visit_rating_advisor##":"Visita al asesor de puntuación"
"##visit_religion_advisor##":"Visita al asesor de religión"
"##visit_trade_advisor##":"Visita al asesor comercial"
"##wages##":"Salarios"
"##wait_for_fishing_boat##":"Estamos esperando a que un astillero nos construya un barco de pesca."
"##waiting_for_free_dock##":"Anclado, esperando a un muelle libre."
"##wall_info##":"Las murallas protegen a los ciudadanos indefensos de los ataques bárbaros. Cuanto más gruesas sean, más embestidas resistirán."
"##wall##":"Muralla"
"##warehouse_full_warning##":"AVISO: Este almacén está completamente lleno. No puede recibir más bienes."
"##warehouse_gettinfull_warning##":"AVISO: Este almacén se está llenando. Solamente puede recibir algunas mercancías ya existentes. No se admitirán nuevos tipos de bienes."
"##warehouse_info##":"Los bienes producidos para el comercio necesitan ser almacenados. Las caravanas de compradores visitan los almacenes para la compra y venta de bienes, y los muelles se suplen de mercancías en los almacenes cercanos."
"##warehouse_low_personal_warning##":"Con poco personal. Puede despachar bienes, pero no recibirlos."
"##warehouse_no_workers##":"Personal escasísimo. Ni se reciben ni se despachan bienes."
"##warehouse_orders##":"Instrucciones del almacén"
"##warehouse##":"Almacén"
"##warehouseman##":"Almacenista"
"##warehouses##":"Almacenes"
"##warning_amphitheater_access##":"Por esta casa no ha pasado ni un actor ni un gladiador en mucho tiempo. Pronto dejará de tener acceso al anfiteatro."
"##warning_barber_access##":"A menos que sea visitada pronto por un barbero, esta casa perderá el acceso a la barbería."
"##warning_baths_access##":"A menos que sea visitada pronto por un empleado de baños, esta casa perderá el acceso a una casa de baños."
"##warning_college_access##":"A menos que sea visitada pronto por un profesor, esta casa perderá el acceso a la academia."
"##warning_colloseum_access##":"Por esta casa no ha pasado ni un domador de leones ni un gladiador en mucho tiempo. Pronto dejará de tener acceso al coliseo."
"##warning_doctor_access##":"Esta casa no ha sido visitada por un médico desde hace mucho tiempo. Pronto perderá el acceso al médico."
"##warning_full##":"Avisos - Activa"
"##warning_hippodrome_access##":"Esta casa no ha sido visitada por un auriga desde hace mucho tiempo. Pronto perderá el acceso al hipódromo."
"##warning_hospital_access##":"A menos que sea visitada pronto por un cirujano, esta casa perderá el acceso al hospital."
"##warning_library_access##":"A menos que sea visitada pronto por un bibliotecario, esta casa perderá el acceso a la biblioteca."
"##warning_school_access##":"A menos que sea visitada pronto por un alumno, esta casa perderá el acceso al colegio."
"##warning_some##":"Avisos - Inactiva"
"##warning_theater_access##":"Esta casa no ha sido visitada por un actor desde hace mucho tiempo. Pronto perderá el acceso al teatro."
"##water_build_tlp##":"Construcciones relacionadas con el agua"
"##water_caption##":"H2O"
"##water_info##":"Infranqueable, aunque se pueden construir algunos puentes. El agua es, a menudo, un nexo comercial imprescindible con el resto del imperio gracias a la construcción de muelles. Los pozos de arcilla deben construirse cerca del agua."
"##water_srvc_fountain_and_well##":"Esta tierra tiene acceso por tuberías a un depósito y agua potable de una fuente o un pozo."
"##water_srvc_reservoir##":"Esta tierra tiene acceso por tuberías a un depósito. El acceso por tuberías es necesario para que funcionen fuentes y baños."
"##water_srvc_well##":"Esta tierra tiene acceso a agua potable."
"##water_supply##":""
"##we_produce_less_than_eat##":"La gente consume más alimentos de los que produce."
"##we_produce_more_than_eat##":"Producimos un poco más de lo que comemos"
"##we_produce_much_than_eat##":"Producimos mucho más de lo que comemos"
"##we_produce_some_than_eat##":"Producimos lo justo para alimentar a todos"
"##weapon##":"Armas"
"##weapons_workshop_bad_work##":"Aquí trabaja muy poco personal. Como resultado, la producción de armas es muy lenta."
"##weapons_workshop_bad_work##":"Con tan poco personal en este taller, la producción es casi inexistente. Producirá muy pocas armas el próximo año."
"##weapons_workshop_full_work##":"Este taller tiene todo el personal que necesita. Se esmera en producir armas."
"##weapons_workshop_info##":"Los herreros transforman el hierro en armas y corazas, que puedes usar para comerciar y obtener altos beneficios o para equipar a tus propias legiones."
"##weapons_workshop_need_resource##":"Este taller necesita recibir hierro de un almacén o de una mina de hierro para producir armas."
"##weapons_workshop_need_some_workers##":"Este taller tiene muy poco personal, por lo que la producción de armas es más lenta de lo deseado."
"##weapons_workshop_patrly_workers##":"Este taller está trabajando por debajo de su capacidad máxima. Como resultado, la producción de armas será algo inferior."
"##weapons_workshop##":"Taller de armas"
"##well_haveno_houses_inarea##":"Este pozo es inservible puesto que no hay casas cerca de él."
"##well_info##":"Los ciudadanos que no tienen acceso a una fuente pueden sacar agua de los pozos, pero las tierras que rodean a un pozo no son muy atractivas para vivir."
"##well##":"Pozo"
"##wharf_full_work##":"Con tantos trabajadores, podemos dar servicio al barco atracado de manera rápida y eficaz."
"##wharf_info##":"Los barcos navegan hasta aquí desde el astillero para recoger a sus tripulaciones y empezar a pescar en las aguas cercanas. Cada dársena proporciona servicio a un barco pesquero."
"##wharf_our_boat_fishing##":"Nuestro barco de pesca se encuentra en un banco de peces, cargando pescado."
"##wharf_our_boat_return##":"Nuestro barco se dirige ahora a la dársena."
"##wharf_out_boat_ready_fishing##":"Nuestro barco de pesca ha salido a buscar bancos de peces."
"##wharf_out_boat_return_with_fish##":"Nuestro barco de pesca regresa de los bancos de peces con la mercancía."
"##wharf##":"Dársena"
"##wheat_farm_bad_work##":"Aquí trabajan muy pocos granjeros. Como resultado, la producción de trigo es lenta."
"##wheat_farm_full_work##":"Esta granja tiene todo el personal que necesita. Saca el máximo beneficio de la tierra cultivada."
"##wheat_farm_info##":"Los cereales son la fuente alimenticia básica de tu pueblo, y la más productiva. Deben almacenarse en graneros para alimentar a tu pueblo, o en almacenes para ser posteriormente exportados."
"##wheat_farm_need_some_workers##":"El personal de esta granja es insuficiente. Sus trabajadores están tardando mucho en cultivar alimentos aquí."
"##wheat_farm_no_workers##":"Esta granja no tiene empleados. No se ha plantado nada."
"##wheat_farm_no_workers##":"Esta granja no tiene empleados. La tierra está estéril."
"##wheat_farm_patrly_workers##":"Esta granja está funcionando por debajo de su capacidad máxima. Sería mas productiva si tuviese mas trabajadores."
"##wheat_farm##":"Granja de trigo"
"##wheat##":"Trigo"
"##wine_workshop_bad_work##":"Con tan poco personal en esta bodega, la producción es casi inexistente. Producirá muy poco vino el próximo año."
"##wine_workshop_full_work##":"Esta bodega tiene todo el personal que necesita. Se esmera en producir vino."
"##wine_workshop_info##":"Los bodegueros convierten las uvas en vino, solicitado por los patricios para la construcción de villas. Se trata de una mercancía muy cotizada."
"##wine_workshop_need_resource##":"Esta bodega no podrá producir vino hasta que reciba uvas de un almacén o de una granja."
"##wine_workshop_no_workers##":"Esta bodega no tiene empleados. La producción se ha detenido."
"##wine_workshop_slow_work##":"Aquí trabaja muy poco personal. Como resultado, la producción de vino es muy lenta."
"##wine_workshop##":"Bodega"
"##wine_workshops_need_some_workers##":"Esta bodega tiene muy poco personal, por lo que la producción de vino es más lenta de lo deseado."
"##wine_workshops_patrly_workers##":"Esta bodega está trabajando por debajo de su capacidad máxima. Como resultado, la producción de vino será algo inferior."
"##wine##":"Vino"
"##wnd_ratings_title##":"Puntuaciones"
"##wndrt_culture##":"Cultura necesaria"
"##wndrt_favor_tooltip##":"Haz clic aquí para asesorarte sobre la puntuación de favor imperial"
"##wndrt_favour##":"Favor"
"##wndrt_peace_tooltip##":"Haz clic aquí para asesorarte sobre la puntuación de paz"
"##wndrt_peace##":"Puntuación de paz"
"##wndrt_prosperity_tooltip##":"Haz clic aquí para asesorarte sobre la puntuación de prosperidad"
"##wndrt_prosperity##":"Prosperidad necesaria"
"##work##":"Funcionando"
"##workers_yearly_wages_is##":"Indice anual estimado de"
"##working_building_need_road##":"AVISO. Este edificio no está operativo. No está al lado de una carretera, por lo que el personal laboral no puede acceder a él."
"##wrath_of_neptune_title##":"Ira de Neptuno"
"##wt_cartPusher##":"Carretero"
"##wt_criminal##":"Criminal"
"##wt_emigrant##":""
"##wt_endeavor##":"Endeavor"
"##wt_engineer##":""
"##wt_gladiator##":"Gladiador"
"##wt_homeless##":"Indigente"
"##wt_immigrant##":"Inmigrante"
"##wt_legioanry##":"Legionario"
"##wt_librarian##":"Bibliotecario"
"##wt_lion_tamer##":"Domador de leones"
"##wt_marketBuyer##":"Cliente de mercado"
"##wt_marketLady##":"Mercader"
"##wt_prefect##":"Prefecto"
"##wt_priest##":"Sacerdote"
"##wt_rioter##":"Amotinado"
"##wt_sheep##":"Ovejas"
"##wt_surgeon##":"Cirujano"
"##wt_taxCollector##":"Recaudador de impuestos"
"##wt_teacher##":"Profesor"
"##year##":"granero contiene"
"##years##":"graneros contienen"
"##yes##":"Sí"
"Variable":"ESPAÑOL (v1.0)"












































































































































































































































































































































































































































































































































































































































































































































































}